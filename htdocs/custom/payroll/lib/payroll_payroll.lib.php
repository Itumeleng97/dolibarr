<?php
/* Copyright (C) 2023 SuperAdmin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    lib/payroll_payroll.lib.php
 * \ingroup payroll
 * \brief   Library files with common functions for Payroll
 */


 require_once DOL_DOCUMENT_ROOT.'/core/db/mysqli.class.php';
/**
 * Prepare array of tabs for Payroll
 *
 * @param	Payroll	$object		Payroll
 * @return 	array					Array of tabs
 */
function myobjectPrepareHead($object)
{
	global $db, $langs, $conf;

	$langs->load("payroll@payroll");

	$showtabofpagecontact = 1;
	$showtabofpagenote = 1;
	$showtabofpagedocument = 1;
	$showtabofpageagenda = 1;

	$h = 0;
	$head = array();

	$head[$h][0] = dol_buildpath("/payroll/myobject_card.php", 1).'?id='.$object->id;
	$head[$h][1] = $langs->trans("Card");
	$head[$h][2] = 'card';
	$h++;

	if ($showtabofpagecontact) {
		$head[$h][0] = dol_buildpath("/payroll/myobject_contact.php", 1).'?id='.$object->id;
		$head[$h][1] = $langs->trans("Contacts");
		$head[$h][2] = 'contact';
		$h++;
	}

	if ($showtabofpagenote) {
		if (isset($object->fields['note_public']) || isset($object->fields['note_private'])) {
			$nbNote = 0;
			if (!empty($object->note_private)) {
				$nbNote++;
			}
			if (!empty($object->note_public)) {
				$nbNote++;
			}
			$head[$h][0] = dol_buildpath('/payroll/myobject_note.php', 1).'?id='.$object->id;
			$head[$h][1] = $langs->trans('Notes');
			if ($nbNote > 0) {
				$head[$h][1] .= (empty($conf->global->MAIN_OPTIMIZEFORTEXTBROWSER) ? '<span class="badge marginleftonlyshort">'.$nbNote.'</span>' : '');
			}
			$head[$h][2] = 'note';
			$h++;
		}
	}

	if ($showtabofpagedocument) {
		require_once DOL_DOCUMENT_ROOT.'/core/lib/files.lib.php';
		require_once DOL_DOCUMENT_ROOT.'/core/class/link.class.php';
		$upload_dir = $conf->payroll->dir_output."/myobject/".dol_sanitizeFileName($object->ref);
		$nbFiles = count(dol_dir_list($upload_dir, 'files', 0, '', '(\.meta|_preview.*\.png)$'));
		$nbLinks = Link::count($db, $object->element, $object->id);
		$head[$h][0] = dol_buildpath("/payroll/myobject_document.php", 1).'?id='.$object->id;
		$head[$h][1] = $langs->trans('Documents');
		if (($nbFiles + $nbLinks) > 0) {
			$head[$h][1] .= '<span class="badge marginleftonlyshort">'.($nbFiles + $nbLinks).'</span>';
		}
		$head[$h][2] = 'document';
		$h++;
	}

	if ($showtabofpageagenda) {
		$head[$h][0] = dol_buildpath("/payroll/myobject_agenda.php", 1).'?id='.$object->id;
		$head[$h][1] = $langs->trans("Events");
		$head[$h][2] = 'agenda';
		$h++;
	}

	// Show more tabs from modules
	// Entries must be declared in modules descriptor with line
	//$this->tabs = array(
	//	'entity:+tabname:Title:@payroll:/payroll/mypage.php?id=__ID__'
	//); // to add new tab
	//$this->tabs = array(
	//	'entity:-tabname:Title:@payroll:/payroll/mypage.php?id=__ID__'
	//); // to remove a tab
	complete_head_from_modules($conf, $langs, $object, $head, $h, 'myobject@payroll');

	complete_head_from_modules($conf, $langs, $object, $head, $h, 'myobject@payroll', 'remove');

	return $head;
}



function fetchUifPercentage() {
    global $db;

    // Array to store the result
    $result = [];

    // SQL query to fetch the row where name is 'PAYROLL_UIF_PERCENTAGE'
    $sql = "SELECT * FROM " . MAIN_DB_PREFIX . "const WHERE name = 'PAYROLL_UIF_PERCENTAGE'";

    // Execute the SQL query
    $resql = $db->query($sql);

    // Check if the query was successful
    if ($resql) {
        // Fetch the result into an associative array
        $result = $db->fetch_array($resql);

    } else {
        // Display an error message if the query fails
        echo "Debug: Query failed\n";
        dol_print_error($db);
    }

    // Return the result
    return $result;
}



function fetchEmployeemed() {
    global $db;

    // Array to store the result
    $result = [];

    // SQL query to fetch the row where name is 'PAYROLL_UIF_PERCENTAGE'
    $sql = "SELECT * FROM " . MAIN_DB_PREFIX . "const WHERE name = 'PAYROLL_EMPLOYEE_MEDICAL_CONT'";


    // Execute the SQL query
    $resql = $db->query($sql);

    // Check if the query was successful
    if ($resql) {
        // Fetch the result into an associative array
        $result = $db->fetch_array($resql);

     
    } else {
        // Display an error message if the query fails
        echo "Debug: Query failed\n";
        dol_print_error($db);
    }

    // Return the result
    return $result;
}



function fetchEmployermed() {
    global $db;

    // Array to store the result
    $result = [];

    // SQL query to fetch the row where name is 'PAYROLL_UIF_PERCENTAGE'
    $sql = "SELECT * FROM " . MAIN_DB_PREFIX . "const WHERE name = 'PAYROLL_EMPLOYER_MEDICAL_CONT'";

    // Debugging: echo the SQL query

    // Execute the SQL query
    $resql = $db->query($sql);

    // Check if the query was successful
    if ($resql) {
        // Fetch the result into an associative array
        $result = $db->fetch_array($resql);


    } else {
        // Display an error message if the query fails
        echo "Debug: Query failed\n";
        dol_print_error($db);
    }

    // Return the result
    return $result;
}



function fetchEmployeeRetire() {
    global $db;

    // Array to store the result
    $result = [];

    // SQL query to fetch the row where name is 'PAYROLL_UIF_PERCENTAGE'
    $sql = "SELECT * FROM " . MAIN_DB_PREFIX . "const WHERE name = 'PAYROLL_EMPLOYEE_RETIREMENT_CONT'";

    
    // Execute the SQL query
    $resql = $db->query($sql);

    // Check if the query was successful
    if ($resql) {
        // Fetch the result into an associative array
        $result = $db->fetch_array($resql);

   
    } else {
        // Display an error message if the query fails
        echo "Debug: Query failed\n";
        dol_print_error($db);
    }

    // Return the result
    return $result;
}



function fetchEmployerRetire() {
    global $db;

    // Array to store the result
    $result = [];

    // SQL query to fetch the row where name is 'PAYROLL_UIF_PERCENTAGE'
    $sql = "SELECT * FROM " . MAIN_DB_PREFIX . "const WHERE name = 'PAYROLL_EMPLOYER_RETIREMENT_CONT'";
  

    // Execute the SQL query
    $resql = $db->query($sql);

    // Check if the query was successful
    if ($resql) {
        // Fetch the result into an associative array
        $result = $db->fetch_array($resql);

      
    } else {
        // Display an error message if the query fails
        echo "Debug: Query failed\n";
        dol_print_error($db);
    }

    // Return the result
    return $result;
}



function fetchIncometax() {
    global $db;

    // Array to store the result
    $result = [];

    // SQL query to fetch the row where name is 'PAYROLL_UIF_PERCENTAGE'
    $sql = "SELECT * FROM " . MAIN_DB_PREFIX . "const WHERE name = 'PAYROLL_EMPLOYEE_INCOME_TAX'";


    // Execute the SQL query
    $resql = $db->query($sql);

    // Check if the query was successful
    if ($resql) {
        // Fetch the result into an associative array
        $result = $db->fetch_array($resql);

       
    } else {
        // Display an error message if the query fails
        echo "Debug: Query failed\n";
        dol_print_error($db);
    }

    // Return the result
    return $result;
}

// function fetchPayrollParams() {
//     global $db;

//     // Array to store payroll parameters
//     $payrollParams = [];

//     // Values to search for
//     $valuesToSearch = ['PAYROLL_UIF_PERCENTAGE', 'PAYROLL_EMPLOYEE_MEDICAL_CONT', 'PAYROLL_EMPLOYER_MEDICAL_CONT', 'PAYROLL_EMPLOYEE_RETIREMENT_CONT', 'PAYROLL_EMPLOYER_RETIREMENT_CONT', 'PAYROLL_EMPLOYEE_INCOME_TAX'];

//     // SQL query to fetch rows based on values in the name column
//     $sql = "SELECT * FROM " . MAIN_DB_PREFIX . "const WHERE name IN ('" . implode("','", $valuesToSearch) . "')";

//     // Debugging: echo the SQL query
//     echo "Debug: SQL Query: $sql\n";

//     // Execute the SQL query
//     $resql = $db->query($sql);

//     // Check if the query was successful
//     if (!$resql) {
//         // Display an error message if the query fails
//         // dol_print_error($db);
//         // return $payrollParams;
//     }

//     // Fetch all results into an associative array
//     while ($row = $db->fetch_assoc($resql)) {
//         $payrollParams[] = $row;
//     }

//     // Debugging: echo the results
//     // echo "Debug: Results: ";
//     // var_dump($payrollParams);

//     // Return the array of payroll parameters
//     return $payrollParams;
// }











function calculateUifDeductions($annual_salary,$uifPercentage, $uifThreshold) {


    // Validate input parameters
    // if (!is_numeric($salary) || $salary < 0 || !is_numeric($uifThreshold) || $uifThreshold < 0 || !is_numeric($uifRate) || $uifRate < 0 || $uifRate > 1) {
    //     // Handle invalid input
    //     return false;
    // }

    // UIF deduction
    $uifarray = [];
    echo $annual_salary.'<br>';
    echo $uifPercentage .'<br>';
    echo $uifThreshold .'<br>';

    $uifDeduction = 0;  // Initialize $uifDeduction


    if ($annual_salary > $uifThreshold) {
        // UIF deduction is a fixed amount if salary is above the threshold

        $uifDeduction = $uifThreshold * $uifPercentage ;

        $uifarray['emp_uif'] = $uifDeduction;
    } else {

        // UIF deduction is a percentage of the salary if below or equal to the threshold
        $uifDeduction = $annual_salary * $uifPercentage;
        $uifarray['emp_uif'] = $uifDeduction;

    }

    $uifarray['total_uif'] = $uifDeduction*2;

    $amount_left = $annual_salary - $uifDeduction;
    $uifarray['amount_left'] = $amount_left;



    return $uifarray;
}


function calculateMedicalContribution($annual_salary, $EmployermedicalCont, $EmployeeMedicaCont) {
    // Validate input parameters
    // if (!is_numeric($salary) || $salary < 0 || !is_numeric($EmployermedicalCont) || $EmployermedicalCont < 0 || $EmployermedicalCont > 1 || !is_numeric($EmployeeMedicaCont) || $EmployeeMedicaCont < 0 || $EmployeeMedicaCont > 1) {
    //     // Handle invalid input
    //     return false;
    // }

    

    $medicalcon = array();


    // Calculate employer contribution
    $employerContribution = $annual_salary * $EmployermedicalCont;

    // Calculate employee contribution
    $employeeContribution = $annual_salary * $EmployeeMedicaCont;

    // Total medical contribution
    $totalContribution = $employerContribution + $employeeContribution; 

    $medicalcon['employerContribution'] = $employerContribution;
    $medicalcon['employeeContribution'] = $employeeContribution;
    $medicalcon['totalContribution'] = $totalContribution;
    $medicalcon['amount_left'] = $annual_salary - $employeeContribution;


        return $medicalcon;
}

function calculateRetirementContribution($annual_salary, $EmployerretirementCont, $EmployeeretirementCont) {
    // Validate input parameters
    // if (!is_numeric($salary) || $salary < 0 || !is_numeric($EmployerretirementCont) || $EmployeeretirementCont < 0 || $EmployerretirementCont > 1 || !is_numeric($EmployeeretirementCont) || $EmployeeretirementCont < 0 || $EmployeeretirementCont > 1) {
    //     // Handle invalid input
    //     return false;
    // }
    $retirement = array();

    // Calculate employer contribution
    $employerContribution = $annual_salary * $EmployerretirementCont;

    // Calculate employee contribution
    $employeeContribution = $annual_salary * $EmployeeretirementCont;

    // Total medical contribution
    $totalContribution = $employerContribution + $employeeContribution;

  
    
    $retirement['employerContribution'] = $employerContribution;
    $retirement['employeeContribution'] = $employeeContribution;
    $retirement['totalContribution'] = $totalContribution;
    $retirement['amount_left']  = $annual_salary - $employeeContribution;


    // Return result
    return $retirement;
}



function calculateIncomeTaxCont($annual_salary, $age ) {
    // // Validate input parameters
    // if (!is_numeric($salary) || $salary < 0 || !is_numeric($EmpIncometax) || $EmpIncometax < 0 || $EmpIncometax > 1 ) {
    //     // Handle invalid input
    //     return false;
    // }




	

	$result = [];

    // Additional conditions based on income tax thresholds
    if ($annual_salary <= 237100) {



		if($age <  65  && $annual_salary < 91250 || $annual_salary < 141250 || $annual_salary < 157900){


			$incomeTax = 0;
        }elseif($age >=  65  && $ageInYears < 75  && $annual_salary < 91250 || $annual_salary < 141250 || $annual_salary < 157900){

        
            $incomeTax = 0;

        }elseif($age > 75  && $annual_salary < 91250 || $annual_salary < 141250 || $annual_salary < 157900){

            $incomeTax = 0;

        }else 
        {

            $incomeTax =(($annual_salary - 91250) * 0.18);

        }

        $result['income_tax'] = $incomeTax;


    } elseif ($annual_salary >= 237101 && $annual_salary <= 370500) {

        $incomeTax = 42678 + (($annual_salary - 237101) * 0.26);


        $result['income_tax'] = $incomeTax;

    } elseif ( $annual_salary >= 370501 && $annual_salary <= 512800) {


        $incomeTax = 77362 + (($annual_salary - 370501) * 0.31);


        $result['income_tax'] = calculateIncomeTaxAmount($annual_salary);


    } elseif ( $annual_salary >= 512801 && $annual_salary <=  673000) {


        $incomeTax = 121475 + (($annual_salary - 512801) * 0.36);

        $result['income_tax'] = $incomeTax;



    } elseif ($annual_salary >= 673001 && $annual_salary <= 857900) {

        $incomeTax = 179147 + (($annual_salary - 673001) * 0.39);


        $result['income_tax'] = $incomeTax;


    } elseif ($annual_salary >= 857901 && $annual_salary <= 1817000) {


        $incomeTax = 251258 + (($annual_salary - 857901) * 0.41);

        $result['income_tax'] = $incomeTax;


    } elseif ($annual_salary >= 1817001 ) {

        $incomeTax = 644489 + (($annual_salary - 1817001) * 0.45);

        $result['income_tax'] = $incomeTax;

    }


        $result['net_pay'] = $annual_salary - $result['income_tax'];

        
       


    // Return the result
    return $result;
}


function totalDeductions($incomeTax = 0 , $retirement,$medical ,$uifDeduction)
{






    
    $totaldeductions =  $incomeTax + $retirement + $medical + $uifDeduction;


    return $totaldeductions;
}

function totalEarnings($overtime_pay,$bonus)
{
    $total_Earnings = $overtime_pay +$bonus;

    return $total_Earnings;
}
