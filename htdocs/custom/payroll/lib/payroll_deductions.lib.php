<?php
/* Copyright (C) 2023 SuperAdmin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    lib/payroll_deductions.lib.php
 * \ingroup payroll
 * \brief   Library files with common functions for Deductions
 */

 require_once DOL_DOCUMENT_ROOT.'/core/db/mysqli.class.php';



/**
 * Prepare array of tabs for Deductions
 *
 * @param	Deductions	$object		Deductions
 * @return 	array					Array of tabs
 */
function myobjectPrepareHead($object)
{
	global $db, $langs, $conf;

	$langs->load("payroll@payroll");

	$showtabofpagecontact = 1;
	$showtabofpagenote = 1;
	$showtabofpagedocument = 1;
	$showtabofpageagenda = 1;

	$h = 0;
	$head = array();

	$head[$h][0] = dol_buildpath("/payroll/myobject_card.php", 1).'?id='.$object->id;
	$head[$h][1] = $langs->trans("Card");
	$head[$h][2] = 'card';
	$h++;

	if ($showtabofpagecontact) {
		$head[$h][0] = dol_buildpath("/payroll/myobject_contact.php", 1).'?id='.$object->id;
		$head[$h][1] = $langs->trans("Contacts");
		$head[$h][2] = 'contact';
		$h++;
	}

	if ($showtabofpagenote) {
		if (isset($object->fields['note_public']) || isset($object->fields['note_private'])) {
			$nbNote = 0;
			if (!empty($object->note_private)) {
				$nbNote++;
			}
			if (!empty($object->note_public)) {
				$nbNote++;
			}
			$head[$h][0] = dol_buildpath('/payroll/myobject_note.php', 1).'?id='.$object->id;
			$head[$h][1] = $langs->trans('Notes');
			if ($nbNote > 0) {
				$head[$h][1] .= (empty($conf->global->MAIN_OPTIMIZEFORTEXTBROWSER) ? '<span class="badge marginleftonlyshort">'.$nbNote.'</span>' : '');
			}
			$head[$h][2] = 'note';
			$h++;
		}
	}

	if ($showtabofpagedocument) {
		require_once DOL_DOCUMENT_ROOT.'/core/lib/files.lib.php';
		require_once DOL_DOCUMENT_ROOT.'/core/class/link.class.php';
		$upload_dir = $conf->payroll->dir_output."/myobject/".dol_sanitizeFileName($object->ref);
		$nbFiles = count(dol_dir_list($upload_dir, 'files', 0, '', '(\.meta|_preview.*\.png)$'));
		$nbLinks = Link::count($db, $object->element, $object->id);
		$head[$h][0] = dol_buildpath("/payroll/myobject_document.php", 1).'?id='.$object->id;
		$head[$h][1] = $langs->trans('Documents');
		if (($nbFiles + $nbLinks) > 0) {
			$head[$h][1] .= '<span class="badge marginleftonlyshort">'.($nbFiles + $nbLinks).'</span>';
		}
		$head[$h][2] = 'document';
		$h++;
	}

	if ($showtabofpageagenda) {
		$head[$h][0] = dol_buildpath("/payroll/myobject_agenda.php", 1).'?id='.$object->id;
		$head[$h][1] = $langs->trans("Events");
		$head[$h][2] = 'agenda';
		$h++;
	}

	// Show more tabs from modules
	// Entries must be declared in modules descriptor with line
	//$this->tabs = array(
	//	'entity:+tabname:Title:@payroll:/payroll/mypage.php?id=__ID__'
	//); // to add new tab
	//$this->tabs = array(
	//	'entity:-tabname:Title:@payroll:/payroll/mypage.php?id=__ID__'
	//); // to remove a tab
	complete_head_from_modules($conf, $langs, $object, $head, $h, 'myobject@payroll');

	complete_head_from_modules($conf, $langs, $object, $head, $h, 'myobject@payroll', 'remove');

	return $head;
}



function fetchUifPercentage() {
    global $db;

    // Array to store the result
    $result = [];

    // SQL query to fetch the row where name is 'PAYROLL_UIF_PERCENTAGE'
    $sql = "SELECT * FROM " . MAIN_DB_PREFIX . "const WHERE name = 'PAYROLL_UIF_PERCENTAGE'";

    // Execute the SQL query
    $resql = $db->query($sql);

    // Check if the query was successful
    if ($resql) {
        // Fetch the result into an associative array
        $result = $db->fetch_array($resql);

    } else {
        // Display an error message if the query fails
        echo "Debug: Query failed\n";
        dol_print_error($db);
    }

    // Return the result
    return $result;
}



function fetchEmployeemed() {
    global $db;

    // Array to store the result
    $result = [];

    // SQL query to fetch the row where name is 'PAYROLL_UIF_PERCENTAGE'
    $sql = "SELECT * FROM " . MAIN_DB_PREFIX . "const WHERE name = 'PAYROLL_EMPLOYEE_MEDICAL_CONT'";


    // Execute the SQL query
    $resql = $db->query($sql);

    // Check if the query was successful
    if ($resql) {
        // Fetch the result into an associative array
        $result = $db->fetch_array($resql);

     
    } else {
        // Display an error message if the query fails
        echo "Debug: Query failed\n";
        dol_print_error($db);
    }

    // Return the result
    return $result;
}



function fetchEmployermed() {
    global $db;

    // Array to store the result
    $result = [];

    // SQL query to fetch the row where name is 'PAYROLL_UIF_PERCENTAGE'
    $sql = "SELECT * FROM " . MAIN_DB_PREFIX . "const WHERE name = 'PAYROLL_EMPLOYER_MEDICAL_CONT'";

    // Debugging: echo the SQL query

    // Execute the SQL query
    $resql = $db->query($sql);

    // Check if the query was successful
    if ($resql) {
        // Fetch the result into an associative array
        $result = $db->fetch_array($resql);


    } else {
        // Display an error message if the query fails
        echo "Debug: Query failed\n";
        dol_print_error($db);
    }

    // Return the result
    return $result;
}



function fetchEmployeeRetire() {
    global $db;

    // Array to store the result
    $result = [];

    // SQL query to fetch the row where name is 'PAYROLL_UIF_PERCENTAGE'
    $sql = "SELECT * FROM " . MAIN_DB_PREFIX . "const WHERE name = 'PAYROLL_EMPLOYEE_RETIREMENT_CONT'";

    
    // Execute the SQL query
    $resql = $db->query($sql);

    // Check if the query was successful
    if ($resql) {
        // Fetch the result into an associative array
        $result = $db->fetch_array($resql);

   
    } else {
        // Display an error message if the query fails
        echo "Debug: Query failed\n";
        dol_print_error($db);
    }

    // Return the result
    return $result;
}



function fetchEmployerRetire() {
    global $db;

    // Array to store the result
    $result = [];

    // SQL query to fetch the row where name is 'PAYROLL_UIF_PERCENTAGE'
    $sql = "SELECT * FROM " . MAIN_DB_PREFIX . "const WHERE name = 'PAYROLL_EMPLOYER_RETIREMENT_CONT'";
  

    // Execute the SQL query
    $resql = $db->query($sql);

    // Check if the query was successful
    if ($resql) {
        // Fetch the result into an associative array
        $result = $db->fetch_array($resql);

      
    } else {
        // Display an error message if the query fails
        echo "Debug: Query failed\n";
        dol_print_error($db);
    }

    // Return the result
    return $result;
}



function fetchIncometax() {
    global $db;

    // Array to store the result
    $result = [];

    // SQL query to fetch the row where name is 'PAYROLL_UIF_PERCENTAGE'
    $sql = "SELECT * FROM " . MAIN_DB_PREFIX . "const WHERE name = 'PAYROLL_EMPLOYEE_INCOME_TAX'";


    // Execute the SQL query
    $resql = $db->query($sql);

    // Check if the query was successful
    if ($resql) {
        // Fetch the result into an associative array
        $result = $db->fetch_array($resql);

       
    } else {
        // Display an error message if the query fails
        echo "Debug: Query failed\n";
        dol_print_error($db);
    }

    // Return the result
    return $result;
}






function calculateIncomeTaxAmount($salary) {
    // Implement your income tax calculation logic here
    // Example: return $salary * 0.1; // Assuming a flat 10% tax rate
    return 0; // Placeholder, replace with your calculation
}
























// function fetchPayrollParams() {
//     global $db;

//     // Array to store payroll parameters
//     $payrollParams = [];

//     // SQL query to fetch payroll parameters from the database
//     $sql = 'SELECT PAYROLL_UIF_PERCENTAGE as Uif , PAYROLL_EMPLOYEE_MEDICAL_CONT as Emed , PAYROLL_EMPLOYER_MEDICAL_CONT as Cmed,';
//     $sql .= ' PAYROLL_EMPLOYEE_RETIREMENT_CONT as Eretirement , PAYROLL_EMPLOYER_RETIREMENT_CONT as Cretirement,';
//     $sql .= ' PAYROLL_EMPLOYEE_INCOME_TAX  as inc_tax';
//     $sql .= ' FROM ' . MAIN_DB_PREFIX . 'const';

//     // Execute the SQL query
//     $resql = $db->query($sql);

//     // Check if the query was successful
//     if ($resql) {
//         // Fetch the results into an associative array
//         $payrollParams = $db->fetch_assoc($resql);
//     }

//     // Return the array of payroll parameters
//     return $payrollParams;
// }
