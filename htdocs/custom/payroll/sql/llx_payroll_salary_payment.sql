-- Copyright (C) 2023 SuperAdmin
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see https://www.gnu.org/licenses/.


CREATE TABLE llx_payroll_salary(
    rowid int AUTO_INCREMENT PRIMARY KEY NOT NULL, 
    ref varchar(30), 
    tms timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
    datec datetime, 
    fk_user integer NOT NULL, 
    datep date, 
    datev date, 
    salary double(24,8), 
    amount double(24,8) NOT NULL, 
    fk_projet integer,
    FOREIGN KEY (fk_projet) REFERENCES llx_projet(rowid),
    fk_typepayment int NOT NULL,
    FOREIGN KEY (fk_typepayment) REFERENCES llx_c_paiement(rowid),
    num_payment varchar(50), 
    label varchar(255), 
    datesp date, 
    dateep date, 
    note text, 
    fk_bank int,
    paye smallint NOT NULL, 
    fk_account int, 
    FOREIGN KEY (fk_account) REFERENCES llx_bank_accout(rowid),
    fk_user_author integer,
    FOREIGN KEY (fk_user_author) REFERENCES llx_user(rowid),
    fk_user_modif integer,
    FOREIGN KEY (fk_user_modif) REFERENCES llx_user(rowid),
    FOREIGN KEY (fk_user) REFERENCES llx_user(rowid)
) ENGINE=innodb;

