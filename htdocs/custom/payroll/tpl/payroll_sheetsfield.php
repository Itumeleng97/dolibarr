<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Document</title>


<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">';
<?php include_once DOL_DOCUMENT_ROOT.'/expensereport/class/expensereport.class.php';
?>

<!-- STYLING -->

    <style>

    body {
        font-family: 'Arial', sans-serif;
    }

    .container-fluid {
        margin-top: 20px;
    }

    .btn-primary,
    .btn-secondary {
        margin-right: 10px;
    }

    h2 {
        color: #333;
    }

    table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        table, th, td {
            border: 1px solid #ddd;
        }

        th, td {
            padding: 10px;
            text-align: left;
        }

        th {
            background-color: #f2f2f2;
        }

        .readonly-input {
        background-color: #f8f8f8; /* Use a light gray background */
        border: 1px solid #ddd;   /* Add a border for better visibility */
    }

</style>
</head>

<body>
  <div class="container-fluid mt-5"  id="paymentView">

<h2 class="mb-4">Current Users Eligible for Payments</h2>
<!-- Employees table -->
<table class="mb-4">
        <thead>
            <tr>
                <th>Staff number</th>
                <th>Name</th>
                <th>Surname</th>
                <th>Position</th>
                <th>Department</th>
                <th>Appointment Type</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>0122</td>
                <td>John</td>
                <td>Doe</td>
                <td>Manager</td>
                <td>HR</td>
                <td>Permanent</td>
            </tr>
            <tr>
                <td>0143</td>
                <td>Jane</td>
                <td>Smith</td>
                <td>Developer</td>
                <td>IT</td>
                <td>Contract</td>
            </tr>

        </tbody>
    </table>
    
  <!-- Buttons -->
  <div class="d-flex mb-3">
    <button class="btn btn-primary mr-6" onclick="showPayOnContract()">Pay On Contract</button>
    <button class="btn btn-secondary" onclick="showPayOnTimesheet()">Pay On Timesheet</button>
  </div>
  

  <!-- Table for Pay on Contract -->
  <div id="payOnContractTable" style="display: none;">
    <h3>Pay on Contract Table</h3>
    
    <?php if ($lines): ?>

<form method="post" action="payroll_card.php?action=runpayroll">

<table class="table table-bordered">
<thead>
  <tr>
    <th>Ref.</th>
    <th>Project</th>
    <th>Job</th>
    <th>weeklyhours</th>
    <th>Hourly_Rate</th>
    <th>Annual_gross_pay</th>
    <th>Gross_pay</th>
    <th>Total_Deductions</th>
    <th>expensereport_amount</th>
    <th>Total_Earnings</th>
    <th>Annual_Tax</th>
    <th>Annual_net_pay</th>
    <th>Net_Pay</th>
    <th>Bonus</th>

  </tr>
</thead>
<tbody>

 

  <!-- Your Pay on Contract table content here -->
<?php foreach ($lines as $line): ?>
<?php 
$expensereport = new ExpenseReport($db);



// echo "Programing is supposed to be fun ".$line['payroll']['fk_user'];
$results = $expensereport->fetchExpenses($line['payroll']['fk_user']);
      

// echo '<pre>';
// var_dump($results);
// echo '<pre>';
$expensereport_amount = 0; // Initialize for each user
$rowid = 0; // Initialize for each user


  
foreach ($results as $result) {
  // Access individual values in the $result array
  $rowid = $result['rowid'];
  $dateApprove = $result['date_approve'];
  $fkUserAuthor = $result['fk_user_author'];
  $totalTtc = $result['total_ttc'];
  $totalTva = $result['total_tva'];


  $expensereport_amount += floatval($totalTtc);

  // Do something with the values...
}







$employee = $emp->fetchPayrollByRowId($line['payroll']['fk_user']); 
$pjt = $project->fetch($line['payroll']['fk_projet']);


$totalDeductions = totalDeductions($line['deductions']['tax_deduction'],$line['deductions']['retirement_deduction'],$line['deductions']['medical_aid_deductions'], $line['deductions']['uif_deduction']);

print '<input type="hidden" name="token" value="'.newToken().'">';
print '<input type="hidden" name="gross_pay[]" value="'.$line['payroll']['gross_pay'].'">';
print '<input type="hidden" name="retirement_deduction[]" value="'.$line['deductions']['retirement_deduction'].'">';
print '<input type="hidden" name="medical_aid_deductions[]" value="'.$line['deductions']['medical_aid_deductions'].'">';
print '<input type="hidden" name="uif_deduction[]" value="'.$line['deductions']['uif_deduction'].'">';
print '<input type="hidden" name="overtime_pay[]" value="'.$line['earnings']['overtime_pay'].'">';
print '<input type="hidden" name="fk_user[]" value="'. $line['payroll']['fk_user'].'">';
print '<input type="hidden" name="fk_payroll[]" value="'. $line['payroll']['rowid'].'">';
print '<input type="hidden" name="fk_expensereport[]" value="'.$rowid.'">';





?>

<tr class="editable-row">


  <td>
      <a href="/htdocs/user/card.php?id=<?php print $line['payroll']['fk_user']; ?>&save_lastsearch_values=1" class="classfortooltip">
          <span class="fas fa-file-projet infobox-proj paddingright" style=""></span>
          <input type="text" name="login[]" value="<?php print $employee->login; ?>" readonly class="readonly-input data-key="login">
      </a>
  </td>
  <td>
      <a href="/htdocs/projet/card.php?id=<?php print $line['payroll']['fk_projet']; ?>&save_lastsearch_values=1" class="classfortooltip">
          <span class="fas fa-file-projet infobox-proj paddingright" style=""></span>
          <input type="text" name="fk_projet[]" value="<?php print $line['payroll']['fk_projet']; ?>" readonly class="readonly-input data-key="fk_projet">
      </a>
  </td>

  <td>
  <input type="text" name="job[]" value="<?php print $employee->job; ?>" readonly class="readonly-input data-key="job">
  </td>
  <td>
  <input type="number" name="weeklyhours[]"  step="0.01" value="<?php print $employee->weeklyhours; ?>" readonly class="readonly-input data-key="weeklyhours">
  </td>
  <td>
  <input type="number" name="rate_per_hour[]" step="0.01" value="<?php print $line['payroll']['rate_per_hour']; ?>" data-key="rate_per_hour" readonly class="readonly-input">
  </td>
  <td>
    <input type="number" name="annual_gross_salary[]" step="0.01" value="<?php print $line['payroll']['annual_gross_salary'];  ?>" readonly class="readonly-input data-key="annual_gross_salary">
 </td>

  <td> 
    <input type="number" name="gross_pay[]" step="0.01" value="<?php print $line['payroll']['gross_pay']; ?>" readonly class="readonly-input data-key="gross_pay">
  </td>

  <td>          
    <input type="number" name="totalDeductions[]" step="0.01" value="<?php print $totalDeductions ?>" readonly class="readonly-input data-key="totalDeductions">
  </td>
  <td>          
    <input type="number" name="expensereport_amount[]" step="0.01" value="<?php print $expensereport_amount ?>" readonly class="readonly-input data-key="expensereport_amount">
  </td>


  <td>         
     <input type="number" name="totalEarnings[]"  step="0.01" value="<?php print totalEarnings($line['earnings']['overtime_pay'],0); ?>" class="editable" data-key="overtime_pay">
  </td>

  <td>   
       <input type="number" name="tax_deduction[]"step="0.01" value="<?php print $line['deductions']['tax_deduction']; ?>" readonly class="readonly-input data-key="tax_deduction">
  </td>
  <td>
  <input type="number" name="annual_net_salary[]" step="0.01"  value="<?php print $line['payroll']['annual_net_salary'];?>" readonly class="readonly-input data-key="annual_net_salary">
  </td>

  <td> 
   <input type="number" name="net_pay[]" step="0.01" value="<?php print $line['payroll']['net_pay'];?>" class="editable" readonly class="readonly-inputdata-key="net_pay">
  </td>

  <td><input type="number" name="bonus[]" step="0.01" value="<?php echo $line['earnings']['bonus']; ?>"  class="editable" data-key="bonus"> </td>

  <td><button class="edit-btn" onclick="editRow(<?php echo $line['payroll']['fk_user']; ?>)">Edit</button></td>

  <td><button class="delete-btn" onclick="deleteRow(<?php echo $line['payroll']['fk_user']; ?>)">Delete</button></td
  <td> 



  

</tr>
<?php endforeach; ?>

</tbody>
</table>
<?php endif; ?>


    <div class="d-flex justify-content-between mb-3">
    <button class="btn btn-primary">Run Payroll</button>
  </div>
</div>

    </form>

    

  </div>

  <!-- Table for Pay on Timesheet -->
  <div id="payOnTimesheetTable" style="display: none;">
    <h3>Pay on Timesheet Table</h3>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Ref.</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Deductions</th>
          <th>Deductions</th>
          <th>Deductions</th>
          <th>Deductions</th>
          <th>Deductions</th>
          <th>Deductions</th>

          <th>Total_Earnings</th>
          <th>Overtime-hours</th>

          <th>Bonus</th>
          <th>Tax</th>
          <th>Net_Pay</th>
          <th>Gross_Pay</th>
        </tr>
      </thead>
      <tbody>
        <!-- Your Pay on Timesheet table content here -->
      </tbody>
    </table>
  </div>

  <script>
    function showPayOnContract() {
      document.getElementById('payOnContractTable').style.display = 'block';
      document.getElementById('payOnTimesheetTable').style.display = 'none';
    }

    function showPayOnTimesheet() {
      document.getElementById('payOnContractTable').style.display = 'none';
      document.getElementById('payOnTimesheetTable').style.display = 'block';
    }

    function showPreviousPayroll() {
  var previousPayroll = document.getElementById('Previouspayroll');

  if (previousPayroll.style.display === 'none') {
    previousPayroll.style.display = 'block';
  } else {
    previousPayroll.style.display = 'none';
  }
}



  </script>


  <div class="d-flex mb-3">
        <button class="btn btn-secondary"  onclick="showPreviousPayroll()">Previous Payroll</button>
    </div>
  
</div>




<div  id="Previouspayroll" class="container-fluid mt-5" style="display: none;">
  <h2 class="mb-4">Previous Users for Payroll</h2>
  <table class="table table-bordered">
    <thead>
      <tr>
          <th>Ref.</th>
          <th>Project</th>
          <th>Job</th>
          <th>annual_gross</th>
          <th>annual_net</th>
          <th>rate_per_hour</th>
          <th>Total_Deductions</th>
          <th>gross_pay</th>
          <th>net_pay</th>
          <th>uif_deduction</th>
          <th>medical_aid_deductions</th>
          <th>retirement_deduction</th>
          <th>tax_deduction</th>
          <th>datecrat</th>
          <th>Bonus</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($payroll_lines as $line): ?>


      <tr class="editable-row">


<td>
    <a href="/htdocs/user/card.php?id=<?php print $line['payroll']['fk_user']; ?>&save_lastsearch_values=1" class="classfortooltip">
        <span class="fas fa-file-projet infobox-proj paddingright" style=""></span>
    </a>        <span class="fas fa-file-projet infobox-proj paddingright"><?php print $line['payroll']['fk_user']; ?></span>
</td>
<td>
    <a href="/htdocs/projet/card.php?id=<?php print $line['payroll']['fk_projet']; ?>&save_lastsearch_values=1" class="classfortooltip">
        <span class="fas fa-file-projet infobox-proj paddingright"><?php print $line['payroll']['fk_projet']; ?></span>
    </a>
</td>


<td>
<span class="fas fa-file-projet infobox-proj paddingright"><?php print $line['payroll']['fk_projet']; ?></span>
</td>
<td>
<span class="fas fa-file-projet infobox-proj paddingright"><?php print $line['payroll']['annual_gross_salary']; ?></span>
</td>
<td>
<span class="fas fa-file-projet infobox-proj paddingright"><?php print $line['payroll']['annual_net_salary']; ?></span>
</td>
<td>
<span class="fas fa-file-projet infobox-proj paddingright"><?php print $line['payroll']['rate_per_hour']; ?></span>
</td>
<td> 
<span class="fas fa-file-projet infobox-proj paddingright"><?php print $line['payroll']['total_deductions']; ?></span>
</td>
<td>   
<span class="fas fa-file-projet infobox-proj paddingright"><?php print $line['payroll']['gross_pay']; ?></span>       
</td>
<td>  
<span class="fas fa-file-projet infobox-proj paddingright"><?php print $line['payroll']['net_pay']; ?></span> 
</td>
<td>   
<span class="fas fa-file-projet infobox-proj paddingright"><?php print $line['payroll']['uif_deduction']; ?></span>
</td>
<td>
<span class="fas fa-file-projet infobox-proj paddingright"><?php print $line['payroll']['medical_aid_deductions']; ?></span>

</td>

<td> 
<span class="fas fa-file-projet infobox-proj paddingright"><?php print $line['payroll']['retirement_deduction']; ?></span>

</td>

<td>
<span class="fas fa-file-projet infobox-proj paddingright"><?php print $line['payroll']['tax_deduction']; ?></span>

</td>
<td>
<span class="fas fa-file-projet infobox-proj paddingright"><?php print $line['payroll']['datec']; ?></span>

</td>
<td>
<span class="fas fa-file-projet infobox-proj paddingright"><?php print $line['payroll']['datec']; ?></span>

</td>
  </tr>
<?php endforeach; ?>


    </tbody>
  </table>



<script>
        document.addEventListener('DOMContentLoaded', function () {
            // Run this code when the DOM is fully loaded
            var editableRows = document.querySelectorAll('.editable-row');

            // Set the initial state to view mode for all editable rows
            editableRows.forEach(function (row) {
                setViewMode(row);
            });
        });

        function editRow(button) {
            var row = button.closest('.editable-row');
            var editables = row.getElementsByClassName('editable');

            // Enable editing for each editable element
            for (var i = 0; i < editables.length; i++) {
                var inputElement = document.createElement('input');
                inputElement.type = 'number';
                inputElement.name = editables[i].getAttribute('data-key');
                inputElement.value = editables[i].innerText;
                editables[i].innerHTML = '';
                editables[i].appendChild(inputElement);
            }

            // Show Save and Cancel buttons, hide Edit and Delete buttons
            showButtons(row, ['save-btn', 'cancel-btn']);
            hideButtons(row, ['edit-btn', 'delete-btn']);
        }

        function saveRow(button) {
            var row = button.closest('.editable-row');
            var inputs = row.querySelectorAll('input');

            // Save the values from input fields and set the view mode
            setViewMode(row);

            // Send the data to the server or update your database here
        }

        function cancelEdit(button) {
            var row = button.closest('.editable-row');
            setViewMode(row);
        }

        function showButtons(row, buttonClasses) {
            for (var i = 0; i < buttonClasses.length; i++) {
                var buttons = row.getElementsByClassName(buttonClasses[i]);
                for (var j = 0; j < buttons.length; j++) {
                    buttons[j].style.display = 'inline-block';
                }
            }
        }

        function hideButtons(row, buttonClasses) {
            for (var i = 0; i < buttonClasses.length; i++) {
                var buttons = row.getElementsByClassName(buttonClasses[i]);
                for (var j = 0; j < buttons.length; j++) {
                    buttons[j].style.display = 'none';
                }
            }
        }

        function setViewMode(row) {
            var editables = row.getElementsByClassName('editable');

            // Hide input fields and show original content for each editable element
            for (var i = 0; i < editables.length; i++) {
                var key = editables[i].getAttribute('data-key');
                var value = editables[i].querySelector('input').value;
                editables[i].innerHTML = value;
            }

            // Show Edit and Delete buttons, hide Save and Cancel buttons
            showButtons(row, ['edit-btn', 'delete-btn']);
            hideButtons(row, ['save-btn', 'cancel-btn']);
        }
    </script>



</body>
</html>

<?php
print '<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>';
print '<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>';
print '<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>';

?>
