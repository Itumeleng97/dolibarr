<?php
/* Copyright (C) 2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2023 SuperAdmin
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *   	\file       myobject_card.php
 *		\ingroup    payroll
 *		\brief      Page to create/edit/view myobject
 */

//if (! defined('NOREQUIREDB'))              define('NOREQUIREDB', '1');				// Do not create database handler $db
//if (! defined('NOREQUIREUSER'))            define('NOREQUIREUSER', '1');				// Do not load object $user
//if (! defined('NOREQUIRESOC'))             define('NOREQUIRESOC', '1');				// Do not load object $mysoc
//if (! defined('NOREQUIRETRAN'))            define('NOREQUIRETRAN', '1');				// Do not load object $langs
//if (! defined('NOSCANGETFORINJECTION'))    define('NOSCANGETFORINJECTION', '1');		// Do not check injection attack on GET parameters
//if (! defined('NOSCANPOSTFORINJECTION'))   define('NOSCANPOSTFORINJECTION', '1');		// Do not check injection attack on POST parameters
//if (! defined('NOTOKENRENEWAL'))           define('NOTOKENRENEWAL', '1');				// Do not roll the Anti CSRF token (used if MAIN_SECURITY_CSRF_WITH_TOKEN is on)
//if (! defined('NOSTYLECHECK'))             define('NOSTYLECHECK', '1');				// Do not check style html tag into posted data
//if (! defined('NOREQUIREMENU'))            define('NOREQUIREMENU', '1');				// If there is no need to load and show top and left menu
//if (! defined('NOREQUIREHTML'))            define('NOREQUIREHTML', '1');				// If we don't need to load the html.form.class.php
//if (! defined('NOREQUIREAJAX'))            define('NOREQUIREAJAX', '1');       	  	// Do not load ajax.lib.php library
//if (! defined("NOLOGIN"))                  define("NOLOGIN", '1');					// If this page is public (can be called outside logged session). This include the NOIPCHECK too.
//if (! defined('NOIPCHECK'))                define('NOIPCHECK', '1');					// Do not check IP defined into conf $dolibarr_main_restrict_ip
//if (! defined("MAIN_LANG_DEFAULT"))        define('MAIN_LANG_DEFAULT', 'auto');					// Force lang to a particular value
//if (! defined("MAIN_AUTHENTICATION_MODE")) define('MAIN_AUTHENTICATION_MODE', 'aloginmodule');	// Force authentication handler
//if (! defined("MAIN_SECURITY_FORCECSP"))   define('MAIN_SECURITY_FORCECSP', 'none');	// Disable all Content Security Policies
//if (! defined('CSRFCHECK_WITH_TOKEN'))     define('CSRFCHECK_WITH_TOKEN', '1');		// Force use of CSRF protection with tokens even for GET
//if (! defined('NOBROWSERNOTIF'))     		 define('NOBROWSERNOTIF', '1');				// Disable browser notification
//if (! defined('NOSESSION'))     		     define('NOSESSION', '1');				    // Disable session

// Load Dolibarr environment
$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) {
	$res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php";
}
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME']; $tmp2 = realpath(__FILE__); $i = strlen($tmp) - 1; $j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) {
	$i--; $j--;
}
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1))."/main.inc.php")) {
	$res = @include substr($tmp, 0, ($i + 1))."/main.inc.php";
}
if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1)))."/main.inc.php")) {
	$res = @include dirname(substr($tmp, 0, ($i + 1)))."/main.inc.php";
}
// Try main.inc.php using relative path
if (!$res && file_exists("../main.inc.php")) {
	$res = @include "../main.inc.php";
}
if (!$res && file_exists("../../main.inc.php")) {
	$res = @include "../../main.inc.php";
}
if (!$res && file_exists("../../../main.inc.php")) {
	$res = @include "../../../main.inc.php";
}
if (!$res) {
	die("Include of main fails");
}

require_once DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/html.formfile.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/html.formprojet.class.php';
include_once DOL_DOCUMENT_ROOT.'/user/class/user.class.php';
include_once DOL_DOCUMENT_ROOT.'/user/class/user.class.php';
include_once DOL_DOCUMENT_ROOT.'/projet/class/project.class.php';
include_once DOL_DOCUMENT_ROOT.'/expensereport/class/expensereport.class.php';


dol_include_once('/payroll/class/salary_payment.class.php');
dol_include_once('/payroll/lib/payroll_payroll.lib.php');
dol_include_once('/payroll/class/payroll_earnings.class.php');
dol_include_once('/payroll/class/deductions.class.php');
dol_include_once('/payroll/class/payroll.class.php');


// Load translation files required by the page
$langs->loadLangs(array("payroll@payroll", "other"));

// Get parameters
$id = GETPOST('id', 'int');
$ref = GETPOST('ref', 'alpha');
$lineid   = GETPOST('lineid', 'int');
$action = GETPOST('action', 'aZ09') ? GETPOST('action', 'aZ09') : $_POST['action'] ?? '';
$confirm = GETPOST('confirm', 'alpha');
$cancel = GETPOST('cancel', 'aZ09');
$contextpage = GETPOST('contextpage', 'aZ') ? GETPOST('contextpage', 'aZ') : str_replace('_', '', basename(dirname(__FILE__)).basename(__FILE__, '.php')); // To manage different context of search
$backtopage = GETPOST('backtopage', 'alpha');					// if not set, a default page will be used
$backtopageforcancel = GETPOST('backtopageforcancel', 'alpha');	// if not set, $backtopage will be used
$backtopagejsfields = GETPOST('backtopagejsfields', 'alpha');
$dol_openinpopup = GETPOST('dol_openinpopup', 'aZ09');



if (!empty($backtopagejsfields)) {
	$tmpbacktopagejsfields = explode(':', $backtopagejsfields);
	$dol_openinpopup = $tmpbacktopagejsfields[0];
}

// Initialize technical objects
$object = new Payroll($db);
$extrafields = new ExtraFields($db);
$diroutputmassaction = $conf->payroll->dir_output.'/temp/massgeneration/'.$user->id;
$hookmanager->initHooks(array('payroll', 'globalcard')); // Note that conf->hooks_modules contains array


// Fetch optionals attributes and labels
$extrafields->fetch_name_optionals_label($object->table_element);

$search_array_options = $extrafields->getOptionalsFromPost($object->table_element, '', 'search_');

// Initialize array of search criterias
$search_all = GETPOST("search_all", 'alpha');
$search = array();
foreach ($object->fields as $key => $val) {
	if (GETPOST('search_'.$key, 'alpha')) {
		$search[$key] = GETPOST('search_'.$key, 'alpha');
	}
}


if (empty($action) && empty($id) && empty($ref)) {
	$action = 'view';
}


// Load object
include DOL_DOCUMENT_ROOT.'/core/actions_fetchobject.inc.php'; // Must be include, not include_once.


// There is several ways to check permission.
// Set $enablepermissioncheck to 1 to enable a minimum low level of checks
$enablepermissioncheck = 0;
if ($enablepermissioncheck) {
	$permissiontoread = $user->hasRight('payroll', 'myobject', 'read');
	$permissiontoadd = $user->hasRight('payroll', 'myobject', 'write'); // Used by the include of actions_addupdatedelete.inc.php and actions_lineupdown.inc.php
	$permissiontodelete = $user->hasRight('payroll', 'myobject', 'delete') || ($permissiontoadd && isset($object->status) && $object->status == $object::STATUS_DRAFT);
	$permissionnote = $user->hasRight('payroll', 'myobject', 'write'); // Used by the include of actions_setnotes.inc.php
	$permissiondellink = $user->hasRight('payroll', 'myobject', 'write'); // Used by the include of actions_dellink.inc.php
} else {
	$permissiontoread = 1;
	$permissiontoadd = 1; // Used by the include of actions_addupdatedelete.inc.php and actions_lineupdown.inc.php
	$permissiontodelete = 1;
	$permissionnote = 1;
	$permissiondellink = 1;
}

$upload_dir = $conf->payroll->multidir_output[isset($object->entity) ? $object->entity : 1].'/payroll';


// Security check (enable the most restrictive one)
//if ($user->socid > 0) accessforbidden();
//if ($user->socid > 0) $socid = $user->socid;
//$isdraft = (isset($object->status) && ($object->status == $object::STATUS_DRAFT) ? 1 : 0);
//restrictedArea($user, $object->module, $object->id, $object->table_element, $object->element, 'fk_soc', 'rowid', $isdraft);
if (!isModEnabled("payroll")) {
	accessforbidden();
}
if (!$permissiontoread) {
	accessforbidden();
}


/*
 * Actions
 */

$parameters = array();
$reshook = $hookmanager->executeHooks('doActions', $parameters, $object, $action); // Note that $action and $object may have been modified by some hooks
if ($reshook < 0) {
	setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');
}

// if (empty($reshook)) {
// 	$error = 0;

// 	$backurlforlist = dol_buildpath('/payroll/payroll_list.php', 1);

// 	if (empty($backtopage) || ($cancel && empty($id))) {
// 		if (empty($backtopage) || ($cancel && strpos($backtopage, '__ID__'))) {
// 			if (empty($id) && (($action != 'add' && $action != 'create') || $cancel)) {
// 				$backtopage = $backurlforlist;
// 			} else {
// 				$backtopage = dol_buildpath('/payroll/payroll_card.php', 1).'?id='.((!empty($id) && $id > 0) ? $id : '__ID__');
// 			}
// 		}
// 	}

// 	$triggermodname = 'PAYROLL_MYOBJECT_MODIFY'; // Name of trigger action code to execute when we modify record

// 	// Actions cancel, add, update, update_extras, confirm_validate, confirm_delete, confirm_deleteline, confirm_clone, confirm_close, confirm_setdraft, confirm_reopen
// 	// include DOL_DOCUMENT_ROOT.'/core/actions_addupdatedelete.inc.php';

// 	// Actions when linking object each other
// 	include DOL_DOCUMENT_ROOT.'/core/actions_dellink.inc.php';

// 	// Actions when printing a doc from card
// 	include DOL_DOCUMENT_ROOT.'/core/actions_printing.inc.php';

// 	// Action to move up and down lines of object
// 	//include DOL_DOCUMENT_ROOT.'/core/actions_lineupdown.inc.php';

// 	// Action to build doc
// 	include DOL_DOCUMENT_ROOT.'/core/actions_builddoc.inc.php';

// 	if ($action == 'set_thirdparty' && $permissiontoadd) {
// 		$object->setValueFrom('fk_soc', GETPOST('fk_soc', 'int'), '', '', 'date', '', $user, $triggermodname);
// 	}
// 	if ($action == 'classin' && $permissiontoadd) {
// 		$object->setProject(GETPOST('projectid', 'int'));
// 	}

// 	// Actions to send emails
// 	$triggersendname = 'PAYROLL_MYOBJECT_SENTBYMAIL';
// 	$autocopy = 'MAIN_MAIL_AUTOCOPY_MYOBJECT_TO';
// 	$trackid = 'myobject'.$object->id;
// 	include DOL_DOCUMENT_ROOT.'/core/actions_sendmails.inc.php';
// }




/*
 * View
 */

$form = new Form($db);
$formfile = new FormFile($db);
$formproject = new FormProjets($db);

$emp = new User($db);

$project = new Project($db);


$expensereport = new ExpenseReport($db);


$pjt = $project->fetch(3);



$title = $langs->trans("Payroll");
$help_url = '';
llxHeader('', $title, $help_url);

// Example : Adding jquery code
// print '<script type="text/javascript">
// jQuery(document).ready(function() {
// 	function init_myfunc()
// 	{
// 		jQuery("#myid").removeAttr(\'disabled\');
// 		jQuery("#myid").attr(\'disabled\',\'disabled\');
// 	}
// 	init_myfunc();
// 	jQuery("#mybutton").click(function() {
// 		init_myfunc();
// 	});
// });
// </script>';

if ($action == 'create') {


	if (empty($permissiontoadd)) {
		accessforbidden('NotEnoughPermissions', 0, 1);
	}

	print load_fiche_titre($langs->trans("NewObject", $langs->transnoentitiesnoconv("Payroll")), '', 'object_'.$object->picto);

	print '<form method="POST" action="payroll_card.php?action=addpayroll">';
	print '<input type="hidden" name="token" value="'.newToken().'">';
	print '<input type="hidden" name="action" value="addpayroll">';
	if ($backtopage) {
		print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';
	}
	if ($backtopageforcancel) {
		print '<input type="hidden" name="backtopageforcancel" value="'.$backtopageforcancel.'">';
	}
	if ($backtopagejsfields) {
		print '<input type="hidden" name="backtopagejsfields" value="'.$backtopagejsfields.'">';
	}
	if ($dol_openinpopup) {
		print '<input type="hidden" name="dol_openinpopup" value="'.$dol_openinpopup.'">';
	}

	print dol_get_fiche_head(array(), '');

	// Set some default values
	//if (! GETPOSTISSET('fieldname')) $_POST['fieldname'] = 'myvalue';

	print '<table class="border centpercent tableforfieldcreate">'."\n";

	// Common attributes 
	include DOL_DOCUMENT_ROOT.'/custom/payroll/tpl/payroll_fields.php';

	// Other attributes
	include DOL_DOCUMENT_ROOT.'/core/tpl/extrafields_add.tpl.php';

	print '</table>'."\n";

	print dol_get_fiche_end();

	print '<button class="btn btn-primary" type="submit" name="save" >Save</button>';

	print '</form>';

	//dol_set_focus('input[name="ref"]');

}



if($action == 'addpayroll'){


	$db->begin();

	



	$payrollPeriodValues = array('Monthly', 'Weekly', 'Semi-monthly', 'Bi-weekly');
	$currencyCodeValues = array('R', 'USD', 'RUB', 'NZD', 'JPY', 'EUR', 'EGP', 'BWP', 'ETB');
	$payroll_types = array('Contract ', 'Timesheets');



	$object->ref = isset($_POST['ref']) ? $_POST['ref'] : 'Ref';
	$object->fk_user = isset($_POST['fk_user']) ? $_POST['fk_user'] : 'User';
	$object->fk_projet = isset($_POST['fk_projet']) ? $_POST['fk_projet'] : 'fk_projet';
	$object->label = isset($_POST['label']) ? $_POST['label'] : 'default_value';
	$object->note = isset($_POST['note']) ? $_POST['note'] : 'default_value';
	$object->fk_user_author = $user->id;
	// $object->fk_user_modif = $user->id;
	$payroll_period = isset($_POST['payroll_period']) ? $_POST['payroll_period'] : '';
	$currency_code = isset($_POST['currency_code']) ? $_POST['currency_code'] : 'currency_code';
	$payroll_type = isset($_POST['payroll_type']) ? $_POST['payroll_type'] : 'payroll_type';
	$object->typepayment = isset($_POST['typepayment']) ? $_POST['typepayment'] : 1;
	$object->payroll_period = $payrollPeriodValues[$payroll_period];
	$object->currency_code = $currencyCodeValues[$currency_code];
	$object->payroll_type = $payroll_types[$payroll_type];


	$datestart = isset($_POST['datestart']) ? $_POST['datestart'] : 'datestart';
	$dateend = isset($_POST['dateend']) ? $_POST['dateend'] : 'dateend';

	// Convert datestart to MySQL format
	$object->datestart = DateTime::createFromFormat('m/d/Y', $datestart)->format('Y-m-d');

	// Convert dateend to MySQL format
	$object->dateend = DateTime::createFromFormat('m/d/Y', $dateend)->format('Y-m-d');



	// 	echo 'Object reference  - '.$object->ref.'<br>';



	$employee = $emp->fetchPayrollByRowId($object->fk_user);

	// echo '<pre>';
	// var_dump($employee);
	// echo '<pre>';




	$birthTimestamp = $employee->birth;

	$currentDate = time();

	$ageInSeconds = $currentDate - $birthTimestamp;

	$ageInYears = floor($ageInSeconds / (365.25 * 24 * 60 * 60));



	$uifPercentage = fetchUifPercentage();
	$fetchEmployerRetire = fetchEmployerRetire();
	$fetchEmployeeRetire = fetchEmployeeRetire();
	$fetchEmployermed = fetchEmployermed();
	$fetchEmployeemed = fetchEmployeemed();

	$Uif_Percentage = $uifPercentage['value']/100;

	$EmployerretirementCont = $fetchEmployerRetire['value']/100;
	$EmployeeretirementCont = $fetchEmployeeRetire['value']/100;
	$EmployermedicalCont = $fetchEmployermed['value']/100;

	$EmployeeMedicaCont = $fetchEmployeemed['value']/100;

 	$deductions = new  Deductions($db);


	$earnings  = new Payroll_earnings($db);


	$salary = $employee->salary;
	$annual_salary = $salary*12;




    $uifThreshold = 241110;
    // // $deductions->fk_payroll =
    $uif_deduction =  calculateUifDeductions($annual_salary,$Uif_Percentage, $uifThreshold);
	$deductions->uif_deduction = $uif_deduction['emp_uif'];
	$amount_left = $uif_deduction['amount_left'];

	$medical =  calculateMedicalContribution($amount_left, $EmployermedicalCont, $EmployeeMedicaCont);

	$amount_left = $medical['amount_left'];

	$retirement = calculateRetirementContribution($amount_left, $EmployerretirementCont,$EmployeeretirementCont);

	$amount_left = $retirement['amount_left'];

	$EmpIncometax = calculateIncomeTaxCont($amount_left, $ageInYears );
	$annual_net_salary = $EmpIncometax['net_pay'];
	$employeeIncometax  = $EmpIncometax['income_tax'];


	$deductions->ref = $object->ref;
	$deductions->fk_projet = $object->fk_projet ;
	$deductions->note_public = $object->note;
	$deductions->fk_user_creat = $user->id;
	
	// $deductions->fk_user_modif = $user->id;
   


	$deductions->medical_aid_deductions = $medical['employeeContribution'];
	$deductions->retirement_deduction = $retirement['employeeContribution'];
	$deductions->tax_deduction = $EmpIncometax['income_tax'] ;
	


	$earnings->ref = $object->ref ;
	$earnings->fk_projet = $object->fk_projet;
	$earnings->fk_user_creat = $user->id;
	$earnings->overtime_hours = 0;
	$earnings->overtime_pay = 0;


	$annual_net_salary = $annual_net_salary;
	$annual_gross_salary = $annual_salary;
	$gross_pay  = $annual_gross_salary/12 ;
	$net_pay	= $annual_net_salary/12;
	$rate_per_hour = $employee->thm;

	$rate_per_hour = number_format($rate_per_hour, 2, '.', '');
	$gross_pay = number_format($gross_pay, 2, '.', '');
	$net_pay = number_format($net_pay, 2, '.', '');
	$annual_net_salary = number_format($annual_net_salary, 2, '.', '');
	$annual_gross_salary = number_format($annual_gross_salary, 2, '.', '');


	



		$results = $object->createpayroll($gross_pay,$net_pay,$rate_per_hour,$annual_net_salary,$annual_gross_salary,$fk_bank ='',$fk_account ='');


		if ($results > 0) {
			$fk_payroll = $results;
			$deductions->fk_payroll = $fk_payroll;


			$results = $deductions->payrolldeductions($user);
			if($results)
			{
				$earnings->fk_payroll = $deductions->fk_payroll;
				
				$res = $earnings->payrollearnings($user);
				if($res)
				{
					echo "SQl query for submiting  payroll earnings work";

					$db->commit();

				}

			}
		} else {

			// Handle the case when createpayroll returns an error (e.g., $results <= 0)
			$db->rollback();

			echo "Error creating payroll record.";
		}



		$lines = $object->fetchpayrollLines();

	
	

	// 	// echo "Nothing is happening";


	require_once DOL_DOCUMENT_ROOT.'/custom/payroll/tpl/payroll_sheetsfield.php';

}


if($action == 'runpayroll'){



	$salaryPayemnt = new Salary_payment($db);



	$postData = $_POST;

	// 	echo '<pre>';
	// var_dump($_POST);
	// echo '<pre>';
	
	// Assuming $postData is the array you provided
	foreach ($postData['login'] as $index => $login) {


    // Now you can process each field for the current row as needed

		$currentBonus = $postData['bonus'][$index];

		// Access each field for the current row using the index
		$currentLogin = $postData['login'][$index];
		$currentFkProjet = $postData['fk_projet'][$index];
		$currentJob = $postData['job'][$index];
		$currentWeeklyHours = $postData['weeklyhours'][$index];
		$currentRatePerHour = $postData['rate_per_hour'][$index];
		$currentAnnualGrossSalary = $postData['annual_gross_salary'][$index];
		$currentGrossPay = $postData['gross_pay'][$index];
		$currentTotalDeductions = (double)$postData['totalDeductions'][$index]/12;
		$currentTotalEarnings = (double)$postData['totalEarnings'][$index];
		$currentTaxDeduction = isset($postData['tax_deduction'][$index]) && $postData['tax_deduction'][$index] > 0 ? $postData['tax_deduction'][$index] / 12 : 0;
		$currentAnnualNetSalary = $postData['annual_net_salary'][$index];
		$retirement_deduction = (double)$postData['retirement_deduction'][$index]/12;
		$medical_aid_deductions = (double)$postData['medical_aid_deductions'][$index]/12;
		$uif_deduction = (double)$postData['uif_deduction'][$index]/12;
		$fk_payroll =  $postData['fk_payroll'][$index];
		$fk_expensereport = $postData['fk_expensereport'][$index];
		$expensereport_amount = $postData['expensereport_amount'][$index];
		$currentNetPay = $postData['net_pay'][$index] + $expensereport_amount ;

		



		$salaryPayemnt->fk_user = $postData['fk_user'][$index];
		$salaryPayemnt->fk_projet = $currentFkProjet;
		$salaryPayemnt->fk_typepayment = 1;
		$salaryPayemnt->num_payment =1;
		$salaryPayemnt->label = $currentLogin ;
		$salaryPayemnt->fk_bank = 1;
		$salaryPayemnt->fk_account = 1;
		$salaryPayemnt->rate_per_hour = $currentRatePerHour;
		$salaryPayemnt->annual_gross_salary = $currentGrossPay;
		$salaryPayemnt->annual_net_salary = $currentAnnualNetSalary;

		$salaryPayemnt->fk_user_author = $user->id;
		$salaryPayemnt->net_pay = $currentNetPay;
		$salaryPayemnt->gross_pay = $currentGrossPay;
		$salaryPayemnt->uif_deduction = $uif_deduction;
		$salaryPayemnt->medical_aid_deductions = $medical_aid_deductions;
		$salaryPayemnt->retirement_deduction = $retirement_deduction;
		$salaryPayemnt->total_earnings = $currentTotalEarnings;
		$salaryPayemnt->total_deductions = $currentTotalDeductions;
		$salaryPayemnt->tax_deduction = $currentTaxDeduction;
		$salaryPayemnt->fk_payroll =  $fk_payroll;
		$salaryPayemnt->fk_expensereport =  $fk_expensereport;
		$salaryPayemnt->expensereport_amount =  $expensereport_amount;







    // For example, you can insert this data into a database or perform other operations

	$results = $salaryPayemnt->runpayroll();

	if(!$results){

		echo "qury fail to run";

	}else
	{
	}


		$payroll_lines = $salaryPayemnt->fetchsalaryLines();
		$lines = $object->fetchpayrollLines();



			

	require_once DOL_DOCUMENT_ROOT.'/custom/payroll/tpl/payroll_sheetsfield.php';

	
	}
}





// }

// Part to create


// Part to edit record
if (($id || $ref) && $action == 'edit') {

	print load_fiche_titre($langs->trans("Payroll"), '', 'object_'.$object->picto);

	print '<form method="POST" action="'.$_SERVER["PHP_SELF"].'">';
	print '<input type="hidden" name="token" value="'.newToken().'">';
	print '<input type="hidden" name="action" value="update">';
	print '<input type="hidden" name="id" value="'.$object->id.'">';
	if ($backtopage) {
		print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';
	}
	if ($backtopageforcancel) {
		print '<input type="hidden" name="backtopageforcancel" value="'.$backtopageforcancel.'">';
	}

	print dol_get_fiche_head();

	print '<table class="border centpercent tableforfieldedit">'."\n";

	// Common attributes
	include DOL_DOCUMENT_ROOT.'/core/tpl/commonfields_edit.tpl.php';

	// Other attributes
	include DOL_DOCUMENT_ROOT.'/core/tpl/extrafields_edit.tpl.php';

	print '</table>';

	print dol_get_fiche_end();

	print $form->buttonsSaveCancel();

	print '</form>';
}

// Part to show record
if ($object->id > 0 && (empty($action) || ($action != 'edit' && $action != 'create'))) {

		
	
}

// End of page
llxFooter();
$db->close();
