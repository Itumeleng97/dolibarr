<?php
/* Copyright (C) 2023 SuperAdmin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

 require_once DOL_DOCUMENT_ROOT.'/core/db/sqlite3.class.php';
 require_once DOL_DOCUMENT_ROOT.'/core/db/mysqli.class.php';
 require_once DOL_DOCUMENT_ROOT.'/core/db/DoliDB.class.php';


 


/**
 * \file    lib/workshop_inspectionlineitem.lib.php
 * \ingroup workshop
 * \brief   Library files with common functions for InspectionLineItem
 */

/**
 * Prepare array of tabs for InspectionLineItem
 *
 * @param	InspectionLineItem	$object		InspectionLineItem
 * @return 	array					Array of tabs
 */
function myobjectPrepareHead($object)
{
	global $db, $langs, $conf;

	$langs->load("workshop@workshop");

	$showtabofpagecontact = 1;
	$showtabofpagenote = 1;
	$showtabofpagedocument = 1;
	$showtabofpageagenda = 1;

	$h = 0;
	$head = array();

	$head[$h][0] = dol_buildpath("/workshop/myobject_card.php", 1).'?id='.$object->id;
	$head[$h][1] = $langs->trans("Card");
	$head[$h][2] = 'card';
	$h++;

	if ($showtabofpagecontact) {
		$head[$h][0] = dol_buildpath("/workshop/myobject_contact.php", 1).'?id='.$object->id;
		$head[$h][1] = $langs->trans("Contacts");
		$head[$h][2] = 'contact';
		$h++;
	}

	if ($showtabofpagenote) {
		if (isset($object->fields['note_public']) || isset($object->fields['note_private'])) {
			$nbNote = 0;
			if (!empty($object->note_private)) {
				$nbNote++;
			}
			if (!empty($object->note_public)) {
				$nbNote++;
			}
			$head[$h][0] = dol_buildpath('/workshop/myobject_note.php', 1).'?id='.$object->id;
			$head[$h][1] = $langs->trans('Notes');
			if ($nbNote > 0) {
				$head[$h][1] .= (empty($conf->global->MAIN_OPTIMIZEFORTEXTBROWSER) ? '<span class="badge marginleftonlyshort">'.$nbNote.'</span>' : '');
			}
			$head[$h][2] = 'note';
			$h++;
		}
	}

	if ($showtabofpagedocument) {
		require_once DOL_DOCUMENT_ROOT.'/core/lib/files.lib.php';
		require_once DOL_DOCUMENT_ROOT.'/core/class/link.class.php';
		$upload_dir = $conf->workshop->dir_output."/myobject/".dol_sanitizeFileName($object->ref);
		$nbFiles = count(dol_dir_list($upload_dir, 'files', 0, '', '(\.meta|_preview.*\.png)$'));
		$nbLinks = Link::count($db, $object->element, $object->id);
		$head[$h][0] = dol_buildpath("/workshop/myobject_document.php", 1).'?id='.$object->id;
		$head[$h][1] = $langs->trans('Documents');
		if (($nbFiles + $nbLinks) > 0) {
			$head[$h][1] .= '<span class="badge marginleftonlyshort">'.($nbFiles + $nbLinks).'</span>';
		}
		$head[$h][2] = 'document';
		$h++;
	}

	if ($showtabofpageagenda) {
		$head[$h][0] = dol_buildpath("/workshop/myobject_agenda.php", 1).'?id='.$object->id;
		$head[$h][1] = $langs->trans("Events");
		$head[$h][2] = 'agenda';
		$h++;
	}

	// Show more tabs from modules
	// Entries must be declared in modules descriptor with line
	//$this->tabs = array(
	//	'entity:+tabname:Title:@workshop:/workshop/mypage.php?id=__ID__'
	//); // to add new tab
	//$this->tabs = array(
	//	'entity:-tabname:Title:@workshop:/workshop/mypage.php?id=__ID__'
	//); // to remove a tab
	complete_head_from_modules($conf, $langs, $object, $head, $h, 'myobject@workshop');

	complete_head_from_modules($conf, $langs, $object, $head, $h, 'myobject@workshop', 'remove');

	return $head;
}




/**
 * Accept an array of form data and insert it into the database
 *
 * @param	Inspectionsheetheader	$object		InspectionLineItem
 * @return 	array					Array of tabs
 */

 function insertInspectionsheet($db, $formData) {

	global $db, $langs, $conf;

    // // Extract the project identifier from 'search_fk_project' and assign it to 'fk_project' as an integer
    // if (isset($formData['search_fk_project'])) {
    //     $search_fk_project = $formData['search_fk_project'];
    //     if (preg_match('/PJ\d+-\d+/', $search_fk_project, $matches)) {
    //         $projectIdentifier = $matches[0];
    //         $last4Digits = substr($projectIdentifier, -4);
    //         $formData['fk_project'] = (int) $last4Digits;
    //     } else {
    //         // If the project identifier pattern is not found, you may handle it as needed
    //         // For example, you can set a default value or display an error message.
    //         // For now, I'll leave it as it is without setting 'fk_project'.
    //     }
    //     unset($formData['search_fk_project']);
    // }

    // Escape and sanitize the remaining form data to prevent SQL injection
    $escapedData = array_map(function($value) use ($db) {
        return $db->escape($value);
    }, $formData);

    // Set the current date and time
    $currentDate = date('Y-m-d H:i:s');

    // Include the date_creation field in the $fields array
    $escapedData['date_creation'] = $currentDate;



    // Construct the SQL query
    $sql = "INSERT INTO ".$db->prefix()."workshop_myobject ( fk_propal, Voltage, Rmp, kilo_watt, Job_no, serial_no, `description`, Make, Inspection, date_creation)
            VALUES ( '{$escapedData['fk_propal']}', '{$escapedData['Voltage']}', '{$escapedData['Rmp']}', '{$escapedData['kilo_watt']}',   '{$escapedData['Job_no']}', '{$escapedData['serial_no']}', '{$escapedData['description']}', '{$escapedData['Make']}', '{$escapedData['Inspection']}', '{$escapedData['date_creation']}')";

    // Execute the query
    $result = $db->query($sql);
    if ($result) {
		
		$lastinsertid = $db->last_insert_id("'$db->prefix()'"."workshop_myobject");
	
    } else {
        echo "Error: " . $db->lasterror();
		// $db->rollback();
		// return -2;
    }

	return $lastinsertid;
}


/**
 * check if record exist in  llx_workshop_myobject
 *
 * @param	product	$object		InspectionLineItem
 * @return 	array					Array of tabs
 */

function recordExists($db, $fk_propal)
{

	echo ":.,................";
    $sql = "SELECT COUNT(*) FROM llx_workshop_myobject WHERE fk_propal = '".$fk_propal."'";
    $result = $db->query($sql);

    if ($result) {
        $count = $db->fetch_array($result);
        return $count[0] > 0;
    } else {
        echo 'Error checking record existence: '.$db->lasterror();
        return false;
    }
}




function inspectionExist($db, $fk_propal)
{
    $sql = "SELECT COUNT(*) AS count FROM llx_workshop_myobject WHERE fk_propal = '".$fk_propal."'";
    $result = $db->query($sql);
    
    if ($result) {
        $row = $db->fetch_object($result);
        $count = $row->count;
        return $count;
    } else {
        echo 'Error checking record existence: '.$db->lasterror();
        return -1;
    }
}



function getWorkshopAndInspectionData($id, $db) {
    // Initialize an array to store the result
    $result = array();

    // Sanitize the ID
    $id = (int)$id;

    // Check if a record exists in llx_workshop_myobject with the given ID
    $sql = "SELECT  rowid ,`description`, date_creation, kilo_watt, serial_no, Rmp, Voltage, Job_no, Make, Inspection
            FROM llx_workshop_myobject
            WHERE fk_propal = " . $id;
    $resql = $db->query($sql);

    if ($resql && $db->num_rows($resql) > 0) {
        // Workshop record found, fetch its data including rowid
        $workshopData = $db->fetch_array($resql);

        // Add workshop data to the result
        $result['workshop'] = $workshopData;

        // Now use the rowid as fk_inspectionsheet to fetch associated records from llx_inspectionlineitem
        $fk_inspectionsheet = $workshopData['rowid'];
        $sql = "SELECT  li.status `status`, li.product_id as prodid , el.label as label , el.product_id as  prodid
                FROM llx_inspectionlineitem As li 
				JOIN  llx_inspectionWorkorderitems AS el ON el.product_id = li.product_id
                WHERE fk_inspectionsheet = " . $fk_inspectionsheet;
        $resql = $db->query($sql);

        if ($resql && $db->num_rows($resql) > 0) {
            // Inspection line items found, fetch and add them to the result
            $inspectionItems = array();
            while ($inspectionData = $db->fetch_array($resql)) {
                $inspectionItems[] = $inspectionData;
            }
            $result['inspection_items'] = $inspectionItems;
        } else {
            // No inspection line items found
            $result['inspection_items'] = array();
        }
    } else {
        // No workshop record found for the given ID
        $result['workshop'] = array();
        $result['inspection_items'] = array();
    }

    return $result;
}







/**
 * select product from db and order by prodid
 *
 * @param	product	$object		InspectionLineItem
 * @return 	array					Array of tabs
 */
  
	// function selectLabelsFromDatabase($db) {


	// 	global $db, $langs, $conf;

	// 	// Query to fetch all the label values from the llx_product table

	// 	// $sql = "SELECT p.label
    //     //     FROM llx_product p
    //     //     INNER JOIN llx_product_extrafields e ON p.rowid = e.fk_object;";
	// 	 $sql = "select lp.rowid as id, lpe.product_id as product_id, lp.label  from llx_product lp inner join llx_product_extrafields lpe on lp.rowid = lpe.fk_object 
	// 				INNER join llx_categorie_product lcp on lcp.fk_product = lp.rowid where lcp.fk_categorie = 6 order by product_id ";
	
	// 	// Execute the query
	// 	$result = $db->query($sql);
	// 	echo "query get executed";
	
	// 	// Check if the query was successful
	// 	if ($result) {
	// 		// Fetch and display each label value
	// 		while ($row = $db->fetch_array($result)) {
	// 			// Access the 'label' column from the row
	// 			$rowid = $row['id'];
	// 			$product_id = $row['product_id'];
	// 			$label = $row['label'];


	// 			// Display the label value
	// 			echo  $product_id . $label . "<br>";
				
	// 		}
	
	// 		// Free the result set
	// 		$db->free($result);
	// 	} else {
	// 		// Query execution failed
	// 		echo "Error: " . $db->lasterror();
	// 	}
	// }


	function selectLabelsFromDatabase($db) {


		global $db, $langs, $conf;

		// Query to fetch all the label values from the llx_product table

		// $sql = "SELECT p.label
        //     FROM llx_product p
        //     INNER JOIN llx_product_extrafields e ON p.rowid = e.fk_object;";
		 $sql = "select  lpe.product_id as product_id,lpe.repair_amount as repair_amount ,lpe.replace_amount as replace_amount, lp.label as label, lp.rowid as fk_product from llx_product lp inner join llx_product_extrafields lpe on lp.rowid = lpe.fk_object 
					INNER join llx_categorie_product lcp on lcp.fk_product = lp.rowid where lcp.fk_categorie = 6 order by product_id ";
	

		$result = $db->query($sql);
	
		// Initialize an empty array to store the results
		$dataArray = array();
	
		// Check if the query was successful
		if ($result) {
			// Fetch each row and add it to the main array
			while ($row = $db->fetch_array($result)) {
				$dataArray[] = $row;
			}
		}
	
		// Return the resulting array
		return $dataArray;
	}




	function selectCausesFromDatabase($db) {


		global $db, $langs, $conf;
		 $sql = "select  causes from llx_inspectioncauseslist";
				
	

		$result = $db->query($sql);
	
		// Initialize an empty array to store the results
		$dataArray = array();
	
		// Check if the query was successful
		
		if ($result) {
			// Fetch each row and add it to the main array
			while ($row = $db->fetch_array($result)) {
				$dataArray[] = $row;
			}
		}
	
		// Return the resulting array
		return $dataArray;
	}
	


	

	function insertinspectionsheetlineitems($db, $fk_inspectionsheet) {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$count = count($_POST);
			
			for ($index = 0; $index < $count; $index++) {
				$product_id = $db->escape($_POST['product_id_' . $index]);
				$fk_product = $db->escape($_POST['fk_product_' . $index]);
				$status = $db->escape($_POST['status_' . $index]);
				$description = $db->escape($_POST['description_' . $index]);
				// $label = $db->escape($_POST['label_' . $index]);
				

				$Date = date('Y-m-d H:i:s');

				// Include the date_creation field in the $fields array
				$currentDate = $db->escape($Date);

			
				
				if ($status === 'Replace' || $status === 'Repair') {
					// echo "We are inside"; // For debugging
					
					$sql = "INSERT INTO ".$db->prefix()."inspectionlineitem (product_id, fk_product, status, fk_inspectionsheet,date_creation)";
					$sql .= " VALUES ('$product_id', '$fk_product', '$status', '$fk_inspectionsheet' ,'$currentDate')";
					
					$result = $db->query($sql);
					
					if ($result) {
					
						
					} else {
						echo "Error: " . $db->lasterror();
					 	

						return -2;
					}
				}
			}
			
		}
	}

	

	

	

/**
 * Accept an array of form data and insert it into the database
 *
 * @param	Inspectionsheetcauses	$object		InspectionLineItem
 * @return 	array					Array of tabs
 */

 function insertInspectionsheetCauses($db, $fk_inspectionsheet) {

	global $db, $langs, $conf;

  
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

		$causesarray = $_POST['causes'];

		foreach ($causesarray as $item) {
		
			$cause = $db->escape($item);
			$currentDate = $db->escape(date('Y-m-d H:i:s'));
		
			// Construct and execute the query
			$sql = "INSERT INTO ".$db->prefix()."inspection_causes (cause, fk_inspectionsheet, date_creation)";
			$sql .= " VALUES ('$cause', '$fk_inspectionsheet', '$currentDate')";
		
			// Execute the query using your database connection
			if ($db->query($sql)) {
				// echo "Insert successful<br>"; // Debugging statement
			} else {
				echo "Insert failed: " . $db->lasterror . "<br>"; // Debugging statement
			}
		}
		
			
		
	}
		
	
}


// function selectInspectionsResults($db) {


// 	global $db, $langs, $conf;

// 	// Query to fetch all the label values from the llx_project_task and llx_inspectionsheet table

// 		$sql = "SELECT il.product_id, il.fk_inspectionsheet, t.label, t.description, il.status FROM llx_inspectionlineitem AS il JOIN llx_projet_task_extrafields AS e
// 				ON il.product_id = e.product_id JOIN llx_projet_task AS t ON e.fk_object = t.rowid WHERE il.fk_inspectionsheet = 366 ORDER BY il.product_id ";  
	
// 	$result = $db->query($sql);

// 	$dataArray = array();

// 	// Check if the query was successful
// 	if ($result) {
	
// 		while ($row = $db->fetch_array($result)) {
// 			$dataArray[] = $row;
// 		}
// 	}
// 	// Return the resulting array
// 	return $dataArray;
// }




function getInspectionWorkOrderItems($db, $fk_propal)
{
    $data = array(); // Initialize the data array

    // Query to get data from llx_workshop_myobject
    $sql = 'SELECT rowid FROM llx_workshop_myobject WHERE fk_propal = '.$fk_propal;
    $result = $db->query($sql);

    if ($result) {
        while ($row = $db->fetch_array($result)) {
            // For each row, query llx_inspectionSheetTasks based on rowid

			//llx_inspectionWorkorderitems
            $inspectionSheetSql = "SELECT li.product_id AS product_id, li.status AS `status`, li.fk_inspectionsheet AS fk_inspectionsheet, wi.product_id As workOrder_prodid, wi.description AS workorder_desc, wi.status AS workorderitem_status , wi.label as label
									FROM llx_inspectionlineitem AS li INNER JOIN llx_inspectionWorkorderitems AS wi ON li.product_id = wi.product_id AND li.fk_inspectionsheet = ".$row['rowid']."
									WHERE li.status = wi.status ORDER BY li.product_id";  

            $inspectionSheetResult = $db->query($inspectionSheetSql);
            
            if ($inspectionSheetResult) {
                while ($inspectionRow = $db->fetch_array($inspectionSheetResult)) {
                    // Combine data from both queries and store in the data array
                    $data[] = array_merge($row, $inspectionRow);
                }
            } else {
                echo 'Error in inspectionSheet query: '.$db->lasterror();
            }
        }
    } else {
        echo 'Error in workshop_myobject query: '.$db->lasterror();
    }

    return $data;
}


/**
 * Accept an array of form data and insert it into the database
 *
 * @param	Inspectionsheetcauses	$object		InspectionLineItem
 * @return 	array					Array of tabs
 */


function insertInspectionSheetWorkorder($db, $data ,$fk_propal)
{




	$currentDate = $db->escape(date('Y-m-d H:i:s'));

	$successfulInsertions = 0;




    foreach ($data as $row) {
        $insertSql = "INSERT INTO llx_inspectionsheet_workorder
            ( fk_Inspectionsheet, fk_propal)
            VALUES
            ('".$row['rowid']."', '".$fk_propal."')";

	

        $result = $db->query($insertSql);

        if ($result) {
      
			$successfulInsertions++; 
        }
		else{
			echo 'Error inserting data..................................: '.$db->lasterror();
		}
}

return $successfulInsertions;

}





/**
 * Accept an array of form data and create it into the database
 *
 * @param	Inspectionsheetcauses	$object		InspectionLineItem
 * @return 	array					Array of tabs
 */


function createWorkorder($db, $fk_propal)
{

    $sql = 'SELECT wm.Inspection, wm.Make, wm.Job_no, wm.serial_no,  wm.Voltage, wm.Rmp, wm.kilo_watt, ist.status, ist.task, ist.product_id, wm.rowid, propal.fk_projet 
	FROM
		llx_workshop_myobject AS wm
	JOIN
		llx_inspectionSheetTasks AS ist ON wm.rowid = ist.fk_inspectionsheet
	JOIN
		llx_propal AS propal ON wm.fk_propal = propal.rowid 
	WHERE
		wm.fk_propal = '.$fk_propal;
	;

    $result = $db->query($sql);

		// var_dump($result);

    if ($result) {
        $data = array(); // Array to store fetched data
        while ($row = $db->fetch_array($result)) {

            $data[] = $row; // Store each row in the data array
        }

        return $data; // Return the data array
    } else {
        echo 'Error: '.$db->lasterror();
        return null;
    }
}



/**
 * Generate a custom project name
 *
 * @param	Inspectionsheetcauses	$object		InspectionLineItem
 * @return 	array					Array of tabs
 */


 function generateCustomReferenceP($lastReference) {
    if (empty($lastReference)) {
        // Get the current year and month
        $year = date('y');
        $month = date('m');
        
        // Check if the last reference is empty, and set a new number accordingly
        $newNumber = '0001';
        
        // Construct the new custom reference with the current year, month, and new number
        $newReference = "PJ{$year}{$month}-{$newNumber}";
    } else {
        // Split the last reference into parts (e.g., "TK2309" and "0014")
        list($prefix, $lastNumber) = explode('-', $lastReference);
        
        // Increment the last number by one
        $newNumber = str_pad((intval($lastNumber) + 1), strlen($lastNumber), '0', STR_PAD_LEFT);
        
        // Combine the prefix and the new number to create the new custom reference
        $newReference = $prefix . '-' . $newNumber;
    }
    
    return $newReference;
}




function generateCustomReferenceT($lastReference) {
    if (empty($lastReference)) {
        // Get the current year and month
        $year = date('y');
        $month = date('m');
        
        // Check if the last reference is empty, and set a new number accordingly
        $newNumber = '0001';
        
        // Construct the new custom reference with the current year, month, and new number
        $newReference = "TK{$year}{$month}-{$newNumber}";
    } else {
        // Split the last reference into parts (e.g., "TK2309" and "0014")
        list($prefix, $lastNumber) = explode('-', $lastReference);
        
        // Increment the last number by one
        $newNumber = str_pad((intval($lastNumber) + 1), strlen($lastNumber), '0', STR_PAD_LEFT);
        
        // Combine the prefix and the new number to create the new custom reference
        $newReference = $prefix . '-' . $newNumber;
    }
    
    return $newReference;
}











function getProjectId($db, $fk_propal) {


    $sql = "SELECT rowid FROM llx_projet WHERE fk_propal = $fk_propal";

    // Execute the SQL query
    $result = $db->query($sql);

    if ($result) {
        // Fetch the first (and only) row directly
        $row = $db->fetch_array($result);

        if ($row) {
            // Extract the rowid
            $rowid = $row['rowid'];

            // Now, $rowid contains the rowid where fk_propal matches $fk_propal
            return $rowid;
        } else {
            // No matching rows found
            return -1; // or any other error code
        }
    } else {
        // Handle the case where the query fails
        return -1; // or any other error code
    }
}

function updatePropal($db, $fk_project, $rowid)
{
    $sql = "UPDATE " . MAIN_DB_PREFIX . "propal SET fk_projet = " . $fk_project . " WHERE rowid = " . (int)$rowid;

    $resql = $db->query($sql);
    if ($resql) {
        return true; // Update was successful
    } else {
        return false; // Update failed
    }
}
