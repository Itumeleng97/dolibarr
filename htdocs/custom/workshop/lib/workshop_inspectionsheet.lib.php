<?php
/* Copyright (C) 2023 SuperAdmin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file    lib/workshop_inspectionsheet.lib.php
 * \ingroup workshop
 * \brief   Library files with common functions for InspectionSheet
 */

/**
 * Prepare array of tabs for InspectionSheet
 *
 * @param	InspectionSheet	$object		InspectionSheet
 * @return 	array					Array of tabs
 */
function myobjectPrepareHead($object)
{
	global $db, $langs, $conf;

	$langs->load("workshop@workshop");

	$showtabofpagecontact = 1;
	$showtabofpagenote = 1;
	$showtabofpagedocument = 1;
	$showtabofpageagenda = 1;

	$h = 0;
	$head = array();

	$head[$h][0] = dol_buildpath("/workshop/myobject_card.php", 1).'?id='.$object->id;
	$head[$h][1] = $langs->trans("Card");
	$head[$h][2] = 'card';
	$h++;

	if ($showtabofpagecontact) {
		$head[$h][0] = dol_buildpath("/workshop/myobject_contact.php", 1).'?id='.$object->id;
		$head[$h][1] = $langs->trans("Contacts");
		$head[$h][2] = 'contact';
		$h++;
	}

	if ($showtabofpagenote) {
		if (isset($object->fields['note_public']) || isset($object->fields['note_private'])) {
			$nbNote = 0;
			if (!empty($object->note_private)) {
				$nbNote++;
			}
			if (!empty($object->note_public)) {
				$nbNote++;
			}
			$head[$h][0] = dol_buildpath('/workshop/myobject_note.php', 1).'?id='.$object->id;
			$head[$h][1] = $langs->trans('Notes');
			if ($nbNote > 0) {
				$head[$h][1] .= (empty($conf->global->MAIN_OPTIMIZEFORTEXTBROWSER) ? '<span class="badge marginleftonlyshort">'.$nbNote.'</span>' : '');
			}
			$head[$h][2] = 'note';
			$h++;
		}
	}

	if ($showtabofpagedocument) {
		require_once DOL_DOCUMENT_ROOT.'/core/lib/files.lib.php';
		require_once DOL_DOCUMENT_ROOT.'/core/class/link.class.php';
		$upload_dir = $conf->workshop->dir_output."/myobject/".dol_sanitizeFileName($object->ref);
		$nbFiles = count(dol_dir_list($upload_dir, 'files', 0, '', '(\.meta|_preview.*\.png)$'));
		$nbLinks = Link::count($db, $object->element, $object->id);
		$head[$h][0] = dol_buildpath("/workshop/myobject_document.php", 1).'?id='.$object->id;
		$head[$h][1] = $langs->trans('Documents');
		if (($nbFiles + $nbLinks) > 0) {
			$head[$h][1] .= '<span class="badge marginleftonlyshort">'.($nbFiles + $nbLinks).'</span>';
		}
		$head[$h][2] = 'document';
		$h++;
	}

	if ($showtabofpageagenda) {
		$head[$h][0] = dol_buildpath("/workshop/myobject_agenda.php", 1).'?id='.$object->id;
		$head[$h][1] = $langs->trans("Events");
		$head[$h][2] = 'agenda';
		$h++;
	}

	// Show more tabs from modules
	// Entries must be declared in modules descriptor with line
	//$this->tabs = array(
	//	'entity:+tabname:Title:@workshop:/workshop/mypage.php?id=__ID__'
	//); // to add new tab
	//$this->tabs = array(
	//	'entity:-tabname:Title:@workshop:/workshop/mypage.php?id=__ID__'
	//); // to remove a tab
	complete_head_from_modules($conf, $langs, $object, $head, $h, 'myobject@workshop');

	complete_head_from_modules($conf, $langs, $object, $head, $h, 'myobject@workshop', 'remove');

	return $head;
}


function selectinspection($db, $fk_propal)
{
    $inspectionData = array();

    $sql = "SELECT inspection, make, description, Job_no FROM ".$db->prefix()."workshop_myobject WHERE fk_propal = $fk_propal"; 

    // Execute the query
    $result = $db->query($sql);
    if ($result) {

	
        while ($row = $db->fetch_array($result)) {
            $inspectionData[] = $row;

        }
    } else {
        echo "Error: " . $db->lasterror();
    }

	// var_dump($inspectionData);

    return $inspectionData;
}




function selectInspectionInfo($db)
{
			$inspectionData = array();
			$sql = "SELECT
			wmo.fk_propal AS fk_propal,
			wmo.Inspection AS Inspection,
			wmo.Job_no AS job_no,
			wmo.Make AS Make,
			wmo.Rmp AS Rmp,
			wmo.serial_no AS serial_no,
			ili.status AS `status`,
			ili.product_id AS product_id,
			ili.fk_inspectionsheet AS fk_inspectionsheet,
			ili.description As `description`
		FROM
			llx_workshop_myobject AS wmo
		INNER JOIN
			llx_inspectionlineitem AS ili ON wmo.rowid = ili.fk_inspectionsheet"; 

		$result = $db->query($sql);

		$dataArray = array();
	
		if ($result) {
			while ($row = $db->fetch_array($result, MYSQLI_ASSOC)) {
				$dataArray[] = $row;
			}
		}


    return $dataArray;
}





function inspectionExist($db, $fk_propal)
{
    $sql = "SELECT COUNT(*) AS count FROM llx_workshop_myobject WHERE fk_propal = '".$fk_propal."'";
    $result = $db->query($sql);
    
    if ($result) {
        $row = $db->fetch_object($result);
        $count = $row->count;
        return $count;
    } else {
        echo 'Error checking record existence: '.$db->lasterror();
        return -1;
    }
}



function getWorkshopAndInspectionData($id, $db) {
    // Initialize an array to store the result
    $result = array();

    // Sanitize the ID
    $id = (int)$id;

    // Check if a record exists in llx_workshop_myobject with the given ID
    $sql = "SELECT  rowid ,`description`, date_creation, kilo_watt, serial_no, Rmp, Voltage, Job_no, Make, Inspection
            FROM llx_workshop_myobject
            WHERE fk_propal = " . $id;
    $resql = $db->query($sql);

    if ($resql && $db->num_rows($resql) > 0) {
        // Workshop record found, fetch its data including rowid
        $workshopData = $db->fetch_array($resql);

        // Add workshop data to the result
        $result['workshop'] = $workshopData;
        // Now use the rowid as fk_inspectionsheet to fetch associated records from llx_inspectionlineitem
        $fk_inspectionsheet = $workshopData['rowid'];
		$sql = "SELECT  el.label as label, el.fk_product as fk_product, el.fk_propal as fk_propal,
				 li.rowid as rowid ,li.fk_inspectionsheet as fk_inspectionsheet, li.status `status`,
				 li.product_id as prodid ,li.fk_product as fk_product
				 FROM llx_inspectionlineitem As li 
				 JOIN  llx_propaldet AS el ON el.fk_product = li.fk_product
				 WHERE fk_propal = " . $id;

        $resql = $db->query($sql);
		if ($resql && $db->num_rows($resql) > 0) {
			// Inspection line items found, fetch and add them to the result
			$inspectionItems = array();
			while ($inspectionData = $db->fetch_array($resql)) {
				$inspectionItem = [
					"status" => $inspectionData["status"],
					"prodid" => $inspectionData["prodid"],
					"label" => $inspectionData["label"],
					"rowid" => $inspectionData["rowid"],
					"fk_product" => $inspectionData["fk_product"]

				];
				$inspectionItems[] = $inspectionItem;
			}
			$result['inspection_items'] = $inspectionItems;
		} else {
			// No inspection line items found
			$result['inspection_items'] = array();
		}
		
		
    } else {
        // No workshop record found for the given ID
        $result['workshop'] = array();
        $result['inspection_items'] = array();
    }

    return $result;
}


function fetchlineitem($fk_propal)
{
    global $db; // Assuming $db is your database connection object

    // Construct the SQL query
    $sql = 'SELECT pd.rowid, pd.fk_propal, pd.fk_parent_line, pd.fk_product , pd.label as custom_label, pd.description, pd.price, pd.qty, pd.vat_src_code, pd.tva_tx,';
    $sql .= ' pd.remise, pd.remise_percent, pd.fk_remise_except, pd.subprice,';
    $sql .= ' pd.info_bits, pd.total_ht, pd.total_tva, pd.total_ttc, pd.fk_product_fournisseur_price as fk_fournprice, pd.buy_price_ht as pa_ht, pd.special_code, pd.rang,';
    $sql .= ' pd.fk_unit,';
    $sql .= ' pd.localtax1_tx, pd.localtax2_tx, pd.total_localtax1, pd.total_localtax2,';
    $sql .= ' pd.fk_multicurrency, pd.multicurrency_code, pd.multicurrency_subprice, pd.multicurrency_total_ht, pd.multicurrency_total_tva, pd.multicurrency_total_ttc,';
    $sql .= ' p.ref as product_ref, p.label as product_label, p.description as product_desc,';
    $sql .= ' pd.date_start, pd.date_end, pd.product_type';
    $sql .= ' FROM ' . MAIN_DB_PREFIX . 'propaldet as pd';
    $sql .= ' LEFT JOIN ' . MAIN_DB_PREFIX . 'product as p ON pd.fk_product = p.rowid';
    $sql .= ' WHERE pd.fk_propal = ' . ((int)$fk_propal);

    $resql = $db->query($sql);

    if ($resql) {
        // Inspection line items found, fetch and add them to the result
        $lineitems = array();
        while ($inspectionData = $db->fetch_array($resql)) {
            $lineitems[] = $inspectionData;
        }
        $result['inspection_items'] = $lineitems;
        $db->free($resql); // Free the result set
    } else {
        // Handle the query error
        $result['error'] = 'Error executing query: ' . $db->lasterror();
        $result['inspection_items'] = array();
    }

    return $result;
}
