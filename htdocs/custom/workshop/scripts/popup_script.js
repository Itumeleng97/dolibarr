// popup_script.js
document.addEventListener("DOMContentLoaded", function() {
      // Define your functions here
      function fetchDataAndShowPopup(id) {

        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'http://localhost/dolibarr-develop/htdocs/custom/modulename/fetch_data.php?id=' + id, true);

        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    var data = JSON.parse(xhr.responseText);
                    showPopupWithData(data);
                } else {
                    console.error('Failed to fetch data');
                }
            }
        };

    xhr.send();


        // Function logic
    }

    function showPopupWithData(data) {

        var popup = document.createElement('div');
        popup.className = 'popup';
        popup.style.position = 'fixed';
        popup.style.top = '25%';
        popup.style.left = '25%';
        popup.style.width = '50%';
        popup.style.height = '50%';
        popup.style.backgroundColor = 'green';
        popup.style.display = 'flex';
        popup.style.justifyContent = 'center';
        popup.style.alignItems = 'center';
        
        var table = document.createElement('table');
        table.className = 'table';
        table.style.width = '100%';
        table.style.border = '1px solid #ddd';
        table.style.borderCollapse = 'collapse';
        
        var headerRow = document.createElement('tr');
        var headerCols = ['Inspection', 'Label', 'Make', 'Description', 'Kilo Watts'];
        
        headerCols.forEach(function (colText) {
            var th = document.createElement('th');
            th.textContent = colText;
            headerRow.appendChild(th);
        });
        
        table.appendChild(headerRow);
        
        data.forEach(function (row) {
            var tr = document.createElement('tr');
            var rowData = [
                row.Inspection, row.label, row.Make, row.description, row.kilo_watt
            ];
            
            rowData.forEach(function (colData) {
                var td = document.createElement('td');
                td.textContent = colData;
                tr.appendChild(td);
            });
            
            table.appendChild(tr);
        });
        
        popup.appendChild(table);
        document.body.appendChild(popup);


        // Function logic
    }

    // Your other JavaScript code can also go here

    // Example usage
    var element = document.getElementById("some-element");
    element.addEventListener("click", function() {
        fetchDataAndShowPopup(123);
    // ...
});
});
