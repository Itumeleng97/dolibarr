<?php
/* Copyright (C) 2017 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2023 SuperAdmin
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *   	\file       inspectionsheet_card.php
 *		\ingroup    workshop
 *		\brief      Page to create/edit/view inspectionsheet
 */

//if (! defined('NOREQUIREDB'))              define('NOREQUIREDB', '1');				// Do not create database handler $db
//if (! defined('NOREQUIREUSER'))            define('NOREQUIREUSER', '1');				// Do not load object $user
//if (! defined('NOREQUIRESOC'))             define('NOREQUIRESOC', '1');				// Do not load object $mysoc
//if (! defined('NOREQUIRETRAN'))            define('NOREQUIRETRAN', '1');				// Do not load object $langs
//if (! defined('NOSCANGETFORINJECTION'))    define('NOSCANGETFORINJECTION', '1');		// Do not check injection attack on GET parameters
//if (! defined('NOSCANPOSTFORINJECTION'))   define('NOSCANPOSTFORINJECTION', '1');		// Do not check injection attack on POST parameters
//if (! defined('NOTOKENRENEWAL'))           define('NOTOKENRENEWAL', '1');				// Do not roll the Anti CSRF token (used if MAIN_SECURITY_CSRF_WITH_TOKEN is on)
//if (! defined('NOSTYLECHECK'))             define('NOSTYLECHECK', '1');				// Do not check style html tag into posted data
//if (! defined('NOREQUIREMENU'))            define('NOREQUIREMENU', '1');				// If there is no need to load and show top and left menu
//if (! defined('NOREQUIREHTML'))            define('NOREQUIREHTML', '1');				// If we don't need to load the html.form.class.php
//if (! defined('NOREQUIREAJAX'))            define('NOREQUIREAJAX', '1');       	  	// Do not load ajax.lib.php library
//if (! defined("NOLOGIN"))                  define("NOLOGIN", '1');					// If this page is public (can be called outside logged session). This include the NOIPCHECK too.
//if (! defined('NOIPCHECK'))                define('NOIPCHECK', '1');					// Do not check IP defined into conf $dolibarr_main_restrict_ip
//if (! defined("MAIN_LANG_DEFAULT"))        define('MAIN_LANG_DEFAULT', 'auto');					// Force lang to a particular value
//if (! defined("MAIN_AUTHENTICATION_MODE")) define('MAIN_AUTHENTICATION_MODE', 'aloginmodule');	// Force authentication handler
//if (! defined("MAIN_SECURITY_FORCECSP"))   define('MAIN_SECURITY_FORCECSP', 'none');	// Disable all Content Security Policies
//if (! defined('CSRFCHECK_WITH_TOKEN'))     define('CSRFCHECK_WITH_TOKEN', '1');		// Force use of CSRF protection with tokens even for GET
//if (! defined('NOBROWSERNOTIF'))     		 define('NOBROWSERNOTIF', '1');				// Disable browser notification
//if (! defined('NOSESSION'))     		     define('NOSESSION', '1');				    // Disable session

// Load Dolibarr environment
$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)





if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) {
	$res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php";
}
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME']; $tmp2 = realpath(__FILE__); $i = strlen($tmp) - 1; $j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) {
	$i--; $j--;
}
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1))."/main.inc.php")) {
	$res = @include substr($tmp, 0, ($i + 1))."/main.inc.php";
}
if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1)))."/main.inc.php")) {
	$res = @include dirname(substr($tmp, 0, ($i + 1)))."/main.inc.php";
}
// Try main.inc.php using relative path
if (!$res && file_exists("../main.inc.php")) {
	$res = @include "../main.inc.php";
}
if (!$res && file_exists("../../main.inc.php")) {
	$res = @include "../../main.inc.php";
}
if (!$res && file_exists("../../../main.inc.php")) {
	$res = @include "../../../main.inc.php";
}
if (!$res) {
	die("Include of main fails");
}


require_once DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/html.formfile.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/html.formprojet.class.php';
require_once DOL_DOCUMENT_ROOT.'/comm/propal/class/propal.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/propal.lib.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/functions2.lib.php';
// require_once DOL_DOCUMENT_ROOT.'/core/class/extrafields.class.php';
// require_once DOL_DOCUMENT_ROOT.'/core/class/doleditor.class.php';
dol_include_once('/workshop/class/inspectionsheet.class.php');
dol_include_once('/workshop/class/inspectionlineitem.class.php');

dol_include_once('/workshop/lib/workshop_inspectionsheet.lib.php');

// Load translation files required by the page
$langs->loadLangs(array("workshop@workshop", "other"));

if (isModEnabled('project')) {
	require_once DOL_DOCUMENT_ROOT.'/projet/class/project.class.php';
	require_once DOL_DOCUMENT_ROOT.'/core/class/html.formprojet.class.php';
}

if (isModEnabled('variants')) {
	require_once DOL_DOCUMENT_ROOT.'/variants/class/ProductCombination.class.php';
}


// Get parameters
$id = GETPOST('id', 'int');
$ref = GETPOST('ref', 'alpha');
$lineid   = GETPOST('lineid', 'int');

$action = GETPOST('action', 'aZ09');
$confirm = GETPOST('confirm', 'alpha');
$cancel = GETPOST('cancel', 'aZ09');
$contextpage = GETPOST('contextpage', 'aZ') ? GETPOST('contextpage', 'aZ') : str_replace('_', '', basename(dirname(__FILE__)).basename(__FILE__, '.php')); // To manage different context of search
$backtopage = GETPOST('backtopage', 'alpha');					// if not set, a default page will be used
$backtopageforcancel = GETPOST('backtopageforcancel', 'alpha');	// if not set, $backtopage will be used
$backtopagejsfields = GETPOST('backtopagejsfields', 'alpha');
$dol_openinpopup = GETPOST('dol_openinpopup', 'aZ09');

if (!empty($backtopagejsfields)) {
	$tmpbacktopagejsfields = explode(':', $backtopagejsfields);
	$dol_openinpopup = $tmpbacktopagejsfields[0];
}

// Initialize technical objects
$object = new InspectionSheet($db);
$extrafields = new ExtraFields($db);
$object_propal = new Propal($db);
$obj_inspectionline = new InspectionLineItem($db);

if ($id > 0 || !empty($ref)) {
	$object_propal->fetch($id, $ref);
}
$diroutputmassaction = $conf->workshop->dir_output.'/temp/massgeneration/'.$user->id;



$langs->loadLangs(array('propal', 'compta', 'bills', 'companies'));






// Security check
$socid = '';
if ($user->socid > 0) {
	$socid = $user->socid;
}



// if ($id > 0 || !empty($ref)) {
// 	$ret = $object_propa->fetch($id, $ref);
// 	if ($ret == 0) {
// 		$langs->load("errors");
// 		setEventMessages($langs->trans('ErrorRecordNotFound'), null, 'errors');
// 		$error++;
// 	} elseif ($ret < 0) {
// 		setEventMessages($object_propa->error, $object_propa->errors, 'errors');
// 		$error++;
// 	}
// }
// if (!$error) {
// 	$object_propa->fetch_thirdparty();
// } else {
// 	header('Location: '.DOL_URL_ROOT.'/comm/propal/list.php');
// 	exit;
// }


$hookmanager->initHooks(array('inspectionsheetcard', 'globalcard')); // Note that conf->hooks_modules contains array

// Fetch optionals attributes and labels
$extrafields->fetch_name_optionals_label($object->table_element);

$search_array_options = $extrafields->getOptionalsFromPost($object->table_element, '', 'search_');

// Initialize array of search criterias
$search_all = GETPOST("search_all", 'alpha');
$search = array();
foreach ($object->fields as $key => $val) {
	if (GETPOST('search_'.$key, 'alpha')) {
		$search[$key] = GETPOST('search_'.$key, 'alpha');
	}
}

if (empty($action) && empty($id) && empty($ref)) {
	$action = 'view';
}

// Load object
include DOL_DOCUMENT_ROOT.'/core/actions_fetchobject.inc.php'; // Must be include, not include_once.

// There is several ways to check permission.
// Set $enablepermissioncheck to 1 to enable a minimum low level of checks
$enablepermissioncheck = 0;
if ($enablepermissioncheck) {
	$permissiontoread = $user->hasRight('workshop', 'inspectionsheet', 'read');
	$permissiontoadd = $user->hasRight('workshop', 'inspectionsheet', 'write'); // Used by the include of actions_addupdatedelete.inc.php and actions_lineupdown.inc.php
	$permissiontodelete = $user->hasRight('workshop', 'inspectionsheet', 'delete') || ($permissiontoadd && isset($object->status) && $object->status == $object::STATUS_DRAFT);
	$permissionnote = $user->hasRight('workshop', 'inspectionsheet', 'write'); // Used by the include of actions_setnotes.inc.php
	$permissiondellink = $user->hasRight('workshop', 'inspectionsheet', 'write'); // Used by the include of actions_dellink.inc.php
} else {
	$permissiontoread = 1;
	$permissiontoadd = 1; // Used by the include of actions_addupdatedelete.inc.php and actions_lineupdown.inc.php
	$permissiontodelete = 1;
	$permissionnote = 1;
	$permissiondellink = 1;
}

$upload_dir = $conf->workshop->multidir_output[isset($object->entity) ? $object->entity : 1].'/inspectionsheet';

// Security check (enable the most restrictive one)
//if ($user->socid > 0) accessforbidden();
//if ($user->socid > 0) $socid = $user->socid;
//$isdraft = (isset($object->status) && ($object->status == $object::STATUS_DRAFT) ? 1 : 0);
//restrictedArea($user, $object->module, $object->id, $object->table_element, $object->element, 'fk_soc', 'rowid', $isdraft);
if (!isModEnabled("workshop")) {
	accessforbidden();
}
if (!$permissiontoread) {
	accessforbidden();
}


/*
 * Actions
 */

$parameters = array();
$reshook = $hookmanager->executeHooks('doActions', $parameters, $object, $action); // Note that $action and $object may have been modified by some hooks
if ($reshook < 0) {
	setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');
}

if (empty($reshook)) {
	$error = 0;

	$backurlforlist = dol_buildpath('/workshop/inspectionsheet_list.php', 1);

	if (empty($backtopage) || ($cancel && empty($id))) {
		if (empty($backtopage) || ($cancel && strpos($backtopage, '__ID__'))) {
			if (empty($id) && (($action != 'add' && $action != 'create') || $cancel)) {
				$backtopage = $backurlforlist;
			} else {
				$backtopage = dol_buildpath('/workshop/inspectionsheet_card.php', 1).'?id='.((!empty($id) && $id > 0) ? $id : '__ID__');
			}
		}
	}

	$triggermodname = 'WORKSHOP_MYOBJECT_MODIFY'; // Name of trigger action code to execute when we modify record

	// Actions cancel, add, update, update_extras, confirm_validate, confirm_delete, confirm_deleteline, confirm_clone, confirm_close, confirm_setdraft, confirm_reopen
	include DOL_DOCUMENT_ROOT.'/core/actions_addupdatedelete.inc.php';

	// Actions when linking object each other
	include DOL_DOCUMENT_ROOT.'/core/actions_dellink.inc.php';

	// Actions when printing a doc from card
	include DOL_DOCUMENT_ROOT.'/core/actions_printing.inc.php';

	// Action to move up and down lines of object
	//include DOL_DOCUMENT_ROOT.'/core/actions_lineupdown.inc.php';

	// Action to build doc
	include DOL_DOCUMENT_ROOT.'/core/actions_builddoc.inc.php';

	if ($action == 'set_thirdparty' && $permissiontoadd) {
		$object->setValueFrom('fk_soc', GETPOST('fk_soc', 'int'), '', '', 'date', '', $user, $triggermodname);
	}
	if ($action == 'classin' && $permissiontoadd) {
		$object->setProject(GETPOST('projectid', 'int'));
	}

	// Actions to send emails
	$triggersendname = 'WORKSHOP_MYOBJECT_SENTBYMAIL';
	$autocopy = 'MAIN_MAIL_AUTOCOPY_MYOBJECT_TO';
	$trackid = 'inspectionsheet'.$object->id;
	include DOL_DOCUMENT_ROOT.'/core/actions_sendmails.inc.php';
}





/*
 * View
 */

$form = new Form($db);
$formfile = new FormFile($db);
$formproject = new FormProjets($db);

$title = $object->ref." - ".$langs->trans('Inspection');

// $title = $langs->trans("InspectionSheet");
$help_url = 'EN:Commercial_Proposals|FR:Proposition_commerciale|ES:Presupuestos';
llxHeader('', $title, $help_url);



// Example : Adding jquery code
// print '<script type="text/javascript">
// jQuery(document).ready(function() {
// 	function init_myfunc()
// 	{
// 		jQuery("#myid").removeAttr(\'disabled\');
// 		jQuery("#myid").attr(\'disabled\',\'disabled\');
// 	}
// 	init_myfunc();
// 	jQuery("#mybutton").click(function() {
// 		init_myfunc();
// 	});
// });
// </script>';


// Part to create


// if ($action == 'create') {



	
    $url = $_SERVER['REQUEST_URI'];
    $urlQuery = parse_url($url, PHP_URL_QUERY);
    parse_str($urlQuery, $queryParameters);
    $fk_propal = isset($queryParameters['id']) ? $queryParameters['id'] : null;

	
    if ($fk_propal !== null) {
		$base_path = DOL_DOCUMENT_ROOT;
		$file_path = '/custom/workshop/inspectionlineitem_card.php';
		$form_action_url = $file_path . '?id=' . $fk_propal. '&action=create';

		$_SESSION['fk_propal'] = $fk_propal;

    }



	$requesturi = $_SERVER['REQUEST_URI'];
	$recordExist = inspectionExist($db, $fk_propal);

	
	$head = propal_prepare_head($object_propal);
	print dol_get_fiche_head($head, 'inspection_sheet', -1, 'propal');
	print '<div class="underbanner clearboth"></div>';

	
	
	
	
	$linkback = '<a href="'.DOL_URL_ROOT.'/comm/propal/list.php?restore_lastsearch_values=1'.(!empty($socid) ? '&socid='.$socid : '').'">'.$langs->trans("BackToList").'</a>';
	
	dol_banner_tab($object_propal, 'ref', $linkback, 1);

	print '<div class="underbanner clearboth"></div>';


if ( $requesturi== '/htdocs/custom/workshop/inspectionsheet_card.php?id=' . $fk_propal  &&  $recordExist == 0){





	
	if (empty($permissiontoadd)) {
		accessforbidden('NotEnoughPermissions', 0, 1);
		
	}

	// print load_fiche_titre($langs->trans("InspectionSheet", $langs->transnoentitiesnoconv("InspectionSheet")), '', 'object_'.$object_propal->picto);

	print '<form method="POST" action="inspectionlineitem_card.php?action=create">';
	print '<input type="hidden" name="token" value="'.newToken().'">';
	print '<input type="hidden" name="action" value="add">';
	// print '<input type="hidden" name="fk_propal" value="' . $id.'">';

	if ($backtopage) {
		print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';
	}
	if ($backtopageforcancel) {
		print '<input type="hidden" name="backtopageforcancel" value="'.$backtopageforcancel.'">';
	}
	if ($backtopagejsfields) {
		print '<input type="hidden" name="backtopagejsfields" value="'.$backtopagejsfields.'">';
	}
	if ($dol_openinpopup) {
		print '<input type="hidden" name="dol_openinpopup" value="'.$dol_openinpopup.'">';
	}

	print dol_get_fiche_head(array(), '');

	// Set some default values
	//if (! GETPOSTISSET('fieldname')) $_POST['fieldname'] = 'myvalue';

	print '<table class="border centpercent tableforfieldcreate">'."\n";

	// Common attributes
	// include DOL_DOCUMENT_ROOT.'/core/tpl/commonfields_add.tpl.php';

	include DOL_DOCUMENT_ROOT.'/custom/workshop/tpl/inspection_sheetFields.php';




	// Other attributes
	include DOL_DOCUMENT_ROOT.'/core/tpl/extrafields_add.tpl.php';

	print '</table>'."\n";

	print $form->buttonsSaveCancel("Create");

	print '</form>';


	
	print '<div class="fichecenter">';
	
	// $cssclass = "titlefield";
	// include DOL_DOCUMENT_ROOT.'/core/tpl/notes.tpl.php';

	print '</div>';






		


	
} elseif($requesturi == '/htdocs/custom/workshop/inspectionsheet_card.php?id=' . $id &&  $recordExist > 0){




		// Store the results in $results
$results = getWorkshopAndInspectionData($fk_propal, $db);

// Access the workshop data from $results
$workshopData = $results['workshop'];



// Access the inspection line items data from $results
$inspectionItems = $results['inspection_items'];


// var_dump($workshopData);
// var_dump($inspectionItems);

include DOL_DOCUMENT_ROOT.'/custom/workshop/tpl/displayInspectionsheet.php';
include DOL_DOCUMENT_ROOT.'/custom/workshop/tpl/inspection_showtable.php';


	}




	//dol_set_focus('input[name="ref"]');
//}
// }

// Part to edit record

		$url = $_SERVER['REQUEST_URI'];
		$urlQuery = parse_url($url, PHP_URL_QUERY);
		parse_str($urlQuery, $queryParameters);
		$fk_propal = isset($queryParameters['id']) ? $queryParameters['id'] : null;
		$action = isset($queryParameters['action']) ? $queryParameters['action'] : null;

if ( $action == 'updateline' && !empty($fk_propal)) {

			// Store the results in $results
			$results = getWorkshopAndInspectionData($fk_propal, $db);

			// Access the workshop data from $results
			$workshopData = $results['workshop'];

			// Access the inspection line items data from $results
			$inspectionItems = $results['inspection_items'];
			// echo '<pre>' . nl2br(var_export($inspectionItems, true)) . '</pre>';

		

			$base_path = DOL_DOCUMENT_ROOT;
			$file_path = '/htdocs/custom/workshop/inspectionsheet_card.php';
			$form_action_url = $file_path . '?id=' . urlencode($fk_propal) . '&action=save';

	print load_fiche_titre($langs->trans("InspectionSheet"), '', 'object_'.$object->picto);

	print '<form method="POST" action="'.$form_action_url.'">';
	print '<input type="hidden" name="token" value="'.newToken().'">';
	print '<input type="hidden" name="action" value="update">';
	print '<input type="hidden" name="fk_propal" value="'.$id.'">';
	if ($backtopage) {
		print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';
	}
	if ($backtopageforcancel) {
		print '<input type="hidden" name="backtopageforcancel" value="'.$backtopageforcancel.'">';
	}

	print dol_get_fiche_head();

	print '<table class="border centpercent tableforfieldedit">'."\n";

	// Common attributes
	include DOL_DOCUMENT_ROOT.'/custom/workshop/tpl/editInspectionsheet_fields.php';

	// Other attributes
	include DOL_DOCUMENT_ROOT.'/core/tpl/extrafields_edit.tpl.php';

	print '</table>';

	print dol_get_fiche_end();
	print '<button type="submit" name="save" >Save</button>';
	// print $form->buttonsSaveCancel();

	print '</form>';

}



if($action == 'save' && !empty($id)){

	// echo '<pre>' . nl2br(var_export($_POST, true)) . '</pre>';


	 $results = fetchlineitem($id);

	

	 $count = count($_POST);


	 $inspectionItems = $results["inspection_items"];

	 $itemsData = [];


	$results = fetchlineitem($id);
	
	$inspectionItems = $results["inspection_items"];
	
	//fk propal

	$fk_propal = $_POST['fk_propal'];
	// Define an array to store data for each item
	$itemsData = [];

	$index = 0; // Initialize an index to count items





	$obj_propal = new Propal($db);
	$propal_line = new PropaleLigne($db);
	$object_project = new project($db);





	// Loop through each item in the "inspection_items" array
	foreach ($inspectionItems as $item) {
		// Extract values from the current item and store them in an associative array
		$itemData = [
			"rowid" => $item["rowid"],
			"fk_propal" => $item["fk_propal"],
			"description" => $item["description"],
			"tva_tx" => $item["tva_tx"],
			// "price_ht" => $item["price_ht"],
			// "price_ttc" => $item["price_ttc"],
			"qty" => $item["qty"],
			"remise" => $item["remise"],
			"remise_percent" => $item["remise_percent"],
			"fk_remise_except" => $item["fk_remise_except"],
			"subprice" => $item["subprice"],
			"info_bits" => $item["info_bits"],
			"total_ht" => $item["total_ht"],
			"total_tva" => $item["total_tva"],
			"total_ttc" => $item["total_ttc"],
			"fk_fournprice" => $item["fk_fournprice"],
			"pa_ht" => $item["pa_ht"],
			"special_code" => $item["special_code"],
			"rang" => $item["rang"],
			"fk_unit" => $item["fk_unit"],
			"localtax1_tx" => $item["localtax1_tx"],
			"localtax2_tx" => $item["localtax2_tx"],
			"total_localtax1" => $item["total_localtax1"],
			"total_localtax2" => $item["total_localtax2"],
			"fk_multicurrency" => $item["fk_multicurrency"],
			"multicurrency_code" => $item["multicurrency_code"],
			"multicurrency_subprice" => $item["multicurrency_subprice"],
			"multicurrency_total_ht" => $item["multicurrency_total_ht"],
			"multicurrency_total_tva" => $item["multicurrency_total_tva"],
			"multicurrency_total_ttc" => $item["multicurrency_total_ttc"],
			"product_ref" => $item["product_ref"],
			"product_label" => $item["product_label"],
			"product_desc" => $item["product_desc"],
			"date_start" => $item["date_start"],
			"date_end" => $item["date_end"],
			"fk_product" => $item["fk_product"],
			"product_type" => $item["product_type"]
		];
		
		// Add the item's data to the itemsData array

		$itemsData[] = $itemData;

	


		// $results = fetch( $item["fk_propal"]);

	

		$product_id = $db->escape($_POST['prodid_' . $index]);
		$fk_product = $db->escape($_POST['fk_product_' . $index]);
		$status = $db->escape($_POST['status_' . $index]);
		// $description = $db->escape($_POST['description_' . $index]);
		$label = $db->escape($_POST['label_' . $index]);
		$linerowid = $db->escape($_POST['rowid_' . $index]);


				if ($status == 'Replace' || $status == 'Repair') {

					

	 		// Clean parameters
			 $description = $item["description"];

			 // Define vat_rate
			 $vat_rate = ($item["tva_tx"] ? $item["tva_tx"] : 0);
			 $vat_rate = str_replace('*', '', $vat_rate);
			 $localtax1_rate = $item["total_localtax1"];
			 $localtax2_rate = $item["total_localtax2"];
			 $pu_ht = price2num($item["total_ht"], '', 2);
			 $pu_ttc = $item["total_ttc"];
	 
			 // Add buying price
			 $fournprice = $item["fk_fournprice"];
			 $buyingprice = price2num($item["pa_ht"] != '' ? $item["pa_ht"] : ''); // If buying_price is '0', we muste keep this value
	 
			 $pu_ht_devise = price2num($item["multicurrency_subprice"], '', 2);
			 $pu_ttc_devise = $item["multicurrency_total_ttc"];
	 
			 $date_start = $item["date_start"];
			 $date_end = $item["date_end"];
	 
			 $remise_percent = $item["remise_percent"];
			 if (empty($remise_percent)) {
				 $remise_percent = 0;
			 }
	 
			 // Prepare a price equivalent for minimum price check
			 $pu_equivalent = $pu_ht;
			 $pu_equivalent_ttc = $pu_ttc;
			 $currency_tx = $item["fk_multicurrency"];
	 
			 // Check if we have a foreing currency
			 // If so, we update the pu_equiv as the equivalent price in base currency
			 if ($pu_ht == '' && $pu_ht_devise != '' && $currency_tx != '') {
				 $pu_equivalent = $pu_ht_devise * $currency_tx;
			 }
			 if ($pu_ttc == '' && $pu_ttc_devise != '' && $currency_tx != '') {
				 $pu_equivalent_ttc = $pu_ttc_devise * $currency_tx;
			 }



		// Extrafields
		$extralabelsline = $extrafields->fetch_name_optionals_label($obj_propal->table_element_line);
		$array_options = $extrafields->getOptionalsFromPost($obj_propal->table_element_line);

			 // Define special_code for special lines
		$special_code = $item["special_code"];
		if (!$item["qty"]) {
			$special_code = 3;
		}

		// Check minimum price
		$productid = $item["fk_product"];
		if (!empty($productid)) {
			$product = new Product($db);
			$res = $product->fetch($productid);

			$type = $product->type;
			$label = $item["product_label"];

			$price_min = $product->price_min;
			if (!empty($conf->global->PRODUIT_MULTIPRICES) && !empty($obj_propal->thirdparty->price_level)) {
				$price_min = $product->multiprices_min[$obj_propal->thirdparty->price_level];
			}
			$price_min_ttc = $product->price_min_ttc;
			if (!empty($conf->global->PRODUIT_MULTIPRICES) && !empty($obj_propal->thirdparty->price_level)) {
				$price_min_ttc = $product->multiprices_min_ttc[$obj_propal->thirdparty->price_level];
			}


		} else {
			$type = $item["product_type"];
			$label = $item["product_label"];
			
		}



		if (!$error) {
			$db->begin();

			if (empty($user->rights->margins->creer)) {
				foreach ($obj_propal->lines as &$line) {
					if ($line->id == $item["rowid"]) {
						$fournprice = $line->fk_fournprice;
						$buyingprice = $line->pa_ht;
						break;
					}
				}
			}

			$qty = $item["qty"];
			$info_bits = $item['info_bits'];

			$pu = $pu_ht;
			$price_base_type = 'HT';
			if (empty($pu) && !empty($pu_ttc)) {
				$pu = $pu_ttc;
				$price_base_type = 'TTC';
			}
		
			$result = $obj_propal->updateline($item["rowid"], $pu, $qty, $remise_percent, $vat_rate, $localtax1_rate, $localtax2_rate, $description, $price_base_type, $info_bits, $special_code, GETPOST('fk_parent_line'), 0, $fournprice, $buyingprice, $label, $type, $date_start, $date_end, $array_options, GETPOST("units"), $pu_ht_devise,$fk_propal);
		
			if ($result >= 0) {
				$db->commit();

				if (empty($conf->global->MAIN_DISABLE_PDF_AUTOUPDATE)) {
					// Define output language
					$outputlangs = $langs;
					if (getDolGlobalInt('MAIN_MULTILANGS')) {
						$outputlangs = new Translate("", $conf);
						$newlang =  'en_US';
						$outputlangs->setDefaultLang($newlang);
					}
					$ret = $obj_propal->fetch($id); // Reload to get new records
					if ($ret > 0) {
						$obj_propal->fetch_thirdparty();
					}
					$obj_propal->generateDocument($obj_propal->model_pdf, $outputlangs, $hidedetails, $hidedesc, $hideref);
				}

			} else {
				$db->rollback();

				setEventMessages($obj_propal->error, $obj_propal->errors, 'errors');
			}


	 

	 }
	} elseif ($status == 'Comply') {

        // Check if the item should be deleted
        $lineid = $item["rowid"];
		
		

        $result = $obj_propal->deleteline($lineid);

        if ($result > 0) {

		
            // Line deleted successfully, reorder lines if needed
            $object->line_order(true);

			$obj_inspectionline->rowid = $linerowid;
			$inspectionline = $obj_inspectionline->deleteline($linerowid );
			
        } else {

            // Failed to delete the line, handle the error or log it
            // You can add error handling logic here
            error_log("Failed to delete line with lineid: $lineid");
        }

		if(!empty($obj_propal->fk_project)){


		}

		

    }


	$index++;

	}
if ($fk_propal !== null) {
	$base_path = DOL_DOCUMENT_ROOT;
	$file_path = '/htdocs/comm/propal/card.php';
	$form_action_url = $file_path . '?id=' . urlencode($fk_propal) ;
}
print '<script>window.location.href = "' . $form_action_url . '";</script>';


// var_dump(count($_POST));
}

// Part to show record
if ($object->id > 0 && (empty($action) || ($action != 'edit' && $action != 'create'))) {
	$head = inspectionsheetPrepareHead($object);

	print dol_get_fiche_head($head, 'card', $langs->trans("InspectionSheet"), -1, $object->picto, 0, '', '', 0, '', 1);

	$formconfirm = '';

	// Confirmation to delete
	if ($action == 'delete') {
		$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"].'?id='.$object->id, $langs->trans('DeleteInspectionSheet'), $langs->trans('ConfirmDeleteObject'), 'confirm_delete', '', 0, 1);
	}
	// Confirmation to delete line
	if ($action == 'deleteline') {
		$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"].'?id='.$object->id.'&lineid='.$lineid, $langs->trans('DeleteLine'), $langs->trans('ConfirmDeleteLine'), 'confirm_deleteline', '', 0, 1);
	}

	// Clone confirmation
	if ($action == 'clone') {
		// Create an array for form
		$formquestion = array();
		$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"].'?id='.$object->id, $langs->trans('ToClone'), $langs->trans('ConfirmCloneAsk', $object->ref), 'confirm_clone', $formquestion, 'yes', 1);
	}

	// Confirmation of action xxxx (You can use it for xxx = 'close', xxx = 'reopen', ...)
	if ($action == 'xxx') {
		$text = $langs->trans('ConfirmActionInspectionSheet', $object->ref);
		/*if (isModEnabled('notification'))
		{
			require_once DOL_DOCUMENT_ROOT . '/core/class/notify.class.php';
			$notify = new Notify($db);
			$text .= '<br>';
			$text .= $notify->confirmMessage('MYOBJECT_CLOSE', $object->socid, $object);
		}*/

		$formquestion = array();

		/*
		$forcecombo=0;
		if ($conf->browser->name == 'ie') $forcecombo = 1;	// There is a bug in IE10 that make combo inside popup crazy
		$formquestion = array(
			// 'text' => $langs->trans("ConfirmClone"),
			// array('type' => 'checkbox', 'name' => 'clone_content', 'label' => $langs->trans("CloneMainAttributes"), 'value' => 1),
			// array('type' => 'checkbox', 'name' => 'update_prices', 'label' => $langs->trans("PuttingPricesUpToDate"), 'value' => 1),
			// array('type' => 'other',    'name' => 'idwarehouse',   'label' => $langs->trans("SelectWarehouseForStockDecrease"), 'value' => $formproduct->selectWarehouses(GETPOST('idwarehouse')?GETPOST('idwarehouse'):'ifone', 'idwarehouse', '', 1, 0, 0, '', 0, $forcecombo))
		);
		*/
		$formconfirm = $form->formconfirm($_SERVER["PHP_SELF"].'?id='.$object->id, $langs->trans('XXX'), $text, 'confirm_xxx', $formquestion, 0, 1, 220);
	}

	// Call Hook formConfirm
	$parameters = array('formConfirm' => $formconfirm, 'lineid' => $lineid);
	$reshook = $hookmanager->executeHooks('formConfirm', $parameters, $object, $action); // Note that $action and $object may have been modified by hook
	if (empty($reshook)) {
		$formconfirm .= $hookmanager->resPrint;
	} elseif ($reshook > 0) {
		$formconfirm = $hookmanager->resPrint;
	}

	// Print form confirm
	print $formconfirm;


	// Object card
	// ------------------------------------------------------------
	$linkback = '<a href="'.dol_buildpath('/workshop/inspectionsheet_list.php', 1).'?restore_lastsearch_values=1'.(!empty($socid) ? '&socid='.$socid : '').'">'.$langs->trans("BackToList").'</a>';

	$morehtmlref = '<div class="refidno">';
	/*
		// Ref customer
		$morehtmlref .= $form->editfieldkey("RefCustomer", 'ref_client', $object->ref_client, $object, $usercancreate, 'string', '', 0, 1);
		$morehtmlref .= $form->editfieldval("RefCustomer", 'ref_client', $object->ref_client, $object, $usercancreate, 'string'.(isset($conf->global->THIRDPARTY_REF_INPUT_SIZE) ? ':'.$conf->global->THIRDPARTY_REF_INPUT_SIZE : ''), '', null, null, '', 1);
		// Thirdparty
		$morehtmlref .= '<br>'.$object->thirdparty->getNomUrl(1, 'customer');
		if (empty($conf->global->MAIN_DISABLE_OTHER_LINK) && $object->thirdparty->id > 0) {
			$morehtmlref .= ' (<a href="'.DOL_URL_ROOT.'/commande/list.php?socid='.$object->thirdparty->id.'&search_societe='.urlencode($object->thirdparty->name).'">'.$langs->trans("OtherOrders").'</a>)';
		}
		// Project
		if (isModEnabled('project')) {
			$langs->load("projects");
			$morehtmlref .= '<br>';
			if ($permissiontoadd) {
				$morehtmlref .= img_picto($langs->trans("Project"), 'project', 'class="pictofixedwidth"');
				if ($action != 'classify') {
					$morehtmlref .= '<a class="editfielda" href="'.$_SERVER['PHP_SELF'].'?action=classify&token='.newToken().'&id='.$object->id.'">'.img_edit($langs->transnoentitiesnoconv('SetProject')).'</a> ';
				}
				$morehtmlref .= $form->form_project($_SERVER['PHP_SELF'].'?id='.$object->id, $object->socid, $object->fk_project, ($action == 'classify' ? 'projectid' : 'none'), 0, 0, 0, 1, '', 'maxwidth300');
			} else {
				if (!empty($object->fk_project)) {
					$proj = new Project($db);
					$proj->fetch($object->fk_project);
					$morehtmlref .= $proj->getNomUrl(1);
					if ($proj->title) {
						$morehtmlref .= '<span class="opacitymedium"> - '.dol_escape_htmltag($proj->title).'</span>';
					}
				}
			}
		}
	*/
	$morehtmlref .= '</div>';


	dol_banner_tab($object, 'ref', $linkback, 1, 'ref', 'ref', $morehtmlref);


	print '<div class="fichecenter">';
	print '<div class="fichehalfleft">';
	print '<div class="underbanner clearboth"></div>';
	print '<table class="border centpercent tableforfield">'."\n";

	// Common attributes
	//$keyforbreak='fieldkeytoswitchonsecondcolumn';	// We change column just before this field
	//unset($object->fields['fk_project']);				// Hide field already shown in banner
	//unset($object->fields['fk_soc']);					// Hide field already shown in banner
	include DOL_DOCUMENT_ROOT.'/core/tpl/commonfields_view.tpl.php';

	// Other attributes. Fields from hook formObjectOptions and Extrafields.
	include DOL_DOCUMENT_ROOT.'/core/tpl/extrafields_view.tpl.php';

	print '</table>';
	print '</div>';
	print '</div>';

	print '<div class="clearboth"></div>';

	print dol_get_fiche_end();


	/*
	 * Lines
	 */

	if (!empty($object->table_element_line)) {
		// Show object lines
		$result = $object->getLinesArray();

		print '	<form name="addproduct" id="addproduct" action="'.$_SERVER["PHP_SELF"].'?id='.$object->id.(($action != 'editline') ? '' : '#line_'.GETPOST('lineid', 'int')).'" method="POST">
		<input type="hidden" name="token" value="' . newToken().'">
		<input type="hidden" name="action" value="' . (($action != 'editline') ? 'addline' : 'updateline').'">
		<input type="hidden" name="mode" value="">
		<input type="hidden" name="page_y" value="">
		
		';

		if (!empty($conf->use_javascript_ajax) && $object->status == 0) {
			include DOL_DOCUMENT_ROOT.'/core/tpl/ajaxrow.tpl.php';
		}

		print '<div class="div-table-responsive-no-min">';
		if (!empty($object->lines) || ($object->status == $object::STATUS_DRAFT && $permissiontoadd && $action != 'selectlines' && $action != 'editline')) {
			print '<table id="tablelines" class="noborder noshadow" width="100%">';
		}

		if (!empty($object->lines)) {
			$object->printObjectLines($action, $mysoc, null, GETPOST('lineid', 'int'), 1);
		}

		// Form to add new line
		if ($object->status == 0 && $permissiontoadd && $action != 'selectlines') {
			if ($action != 'editline') {
				// Add products/services form

				$parameters = array();
				$reshook = $hookmanager->executeHooks('formAddObjectLine', $parameters, $object, $action); // Note that $action and $object may have been modified by hook
				if ($reshook < 0) setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');
				if (empty($reshook))
					$object->formAddObjectLine(1, $mysoc, $soc);
			}
		}

		if (!empty($object->lines) || ($object->status == $object::STATUS_DRAFT && $permissiontoadd && $action != 'selectlines' && $action != 'editline')) {
			print '</table>';
		}
		print '</div>';

		print "</form>\n";
	}


	// Buttons for actions

	if ($action != 'presend' && $action != 'editline') {
		print '<div class="tabsAction">'."\n";
		$parameters = array();
		$reshook = $hookmanager->executeHooks('addMoreActionsButtons', $parameters, $object, $action); // Note that $action and $object may have been modified by hook
		if ($reshook < 0) {
			setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');
		}

		if (empty($reshook)) {
			// Send
			if (empty($user->socid)) {
				print dolGetButtonAction('', $langs->trans('SendMail'), 'default', $_SERVER["PHP_SELF"].'?id='.$object->id.'&action=presend&token='.newToken().'&mode=init#formmailbeforetitle');
			}

			// Back to draft
			if ($object->status == $object::STATUS_VALIDATED) {
				print dolGetButtonAction('', $langs->trans('SetToDraft'), 'default', $_SERVER["PHP_SELF"].'?id='.$object->id.'&action=confirm_setdraft&confirm=yes&token='.newToken(), '', $permissiontoadd);
			}

			print dolGetButtonAction('', $langs->trans('Modify'), 'default', $_SERVER["PHP_SELF"].'?id='.$object->id.'&action=edit&token='.newToken(), '', $permissiontoadd);

			// Validate
			if ($object->status == $object::STATUS_DRAFT) {
				if (empty($object->table_element_line) || (is_array($object->lines) && count($object->lines) > 0)) {
					print dolGetButtonAction('', $langs->trans('Validate'), 'default', $_SERVER['PHP_SELF'].'?id='.$object->id.'&action=confirm_validate&confirm=yes&token='.newToken(), '', $permissiontoadd);
				} else {
					$langs->load("errors");
					print dolGetButtonAction($langs->trans("ErrorAddAtLeastOneLineFirst"), $langs->trans("Validate"), 'default', '#', '', 0);
				}
			}

			// Clone
			if ($permissiontoadd) {
				print dolGetButtonAction('', $langs->trans('ToClone'), 'default', $_SERVER['PHP_SELF'].'?id='.$object->id.(!empty($object->socid)?'&socid='.$object->socid:'').'&action=clone&token='.newToken(), '', $permissiontoadd);
			}

			/*
			if ($permissiontoadd) {
				if ($object->status == $object::STATUS_ENABLED) {
					print dolGetButtonAction('', $langs->trans('Disable'), 'default', $_SERVER['PHP_SELF'].'?id='.$object->id.'&action=disable&token='.newToken(), '', $permissiontoadd);
				} else {
					print dolGetButtonAction('', $langs->trans('Enable'), 'default', $_SERVER['PHP_SELF'].'?id='.$object->id.'&action=enable&token='.newToken(), '', $permissiontoadd);
				}
			}
			if ($permissiontoadd) {
				if ($object->status == $object::STATUS_VALIDATED) {
					print dolGetButtonAction('', $langs->trans('Cancel'), 'default', $_SERVER['PHP_SELF'].'?id='.$object->id.'&action=close&token='.newToken(), '', $permissiontoadd);
				} else {
					print dolGetButtonAction('', $langs->trans('Re-Open'), 'default', $_SERVER['PHP_SELF'].'?id='.$object->id.'&action=reopen&token='.newToken(), '', $permissiontoadd);
				}
			}
			*/

			// Delete
			$params = array();
			print dolGetButtonAction('', $langs->trans("Delete"), 'delete', $_SERVER["PHP_SELF"].'?id='.$object->id.'&action=delete&token='.newToken(), 'delete', $permissiontodelete, $params);
		}
		print '</div>'."\n";
	}


	// Select mail models is same action as presend
	if (GETPOST('modelselected')) {
		$action = 'presend';
	}

	if ($action != 'presend') {
		print '<div class="fichecenter"><div class="fichehalfleft">';
		print '<a name="builddoc"></a>'; // ancre

		$includedocgeneration = 1;

		// Documents
		if ($includedocgeneration) {
			$objref = dol_sanitizeFileName($object->ref);
			$relativepath = $objref.'/'.$objref.'.pdf';
			$filedir = $conf->workshop->dir_output.'/'.$object->element.'/'.$objref;
			$urlsource = $_SERVER["PHP_SELF"]."?id=".$object->id;
			$genallowed = $permissiontoread; // If you can read, you can build the PDF to read content
			$delallowed = $permissiontoadd; // If you can create/edit, you can remove a file on card
			print $formfile->showdocuments('workshop:InspectionSheet', $object->element.'/'.$objref, $filedir, $urlsource, $genallowed, $delallowed, $object->model_pdf, 1, 0, 0, 28, 0, '', '', '', $langs->defaultlang);
		}

		// Show links to link elements
		$linktoelem = $form->showLinkToObjectBlock($object, null, array('inspectionsheet'));
		$somethingshown = $form->showLinkedObjectBlock($object, $linktoelem);


		print '</div><div class="fichehalfright">';

		$MAXEVENT = 10;

		$morehtmlcenter = dolGetButtonTitle($langs->trans('SeeAll'), '', 'fa fa-bars imgforviewmode', dol_buildpath('/workshop/inspectionsheet_agenda.php', 1).'?id='.$object->id);

		// List of actions on element
		include_once DOL_DOCUMENT_ROOT.'/core/class/html.formactions.class.php';
		$formactions = new FormActions($db);
		$somethingshown = $formactions->showactions($object, $object->element.'@'.$object->module, (is_object($object->thirdparty) ? $object->thirdparty->id : 0), 1, '', $MAXEVENT, '', $morehtmlcenter);

		print '</div></div>';
	}

	//Select mail models is same action as presend
	if (GETPOST('modelselected')) {
		$action = 'presend';
	}

	// Presend form
	$modelmail = 'inspectionsheet';
	$defaulttopic = 'InformationMessage';
	$diroutput = $conf->workshop->dir_output;
	$trackid = 'inspectionsheet'.$object->id;

	include DOL_DOCUMENT_ROOT.'/core/tpl/card_presend.tpl.php';
}

// End of page
llxFooter();
$db->close();
