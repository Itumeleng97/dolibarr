 <!DOCTYPE html>
<html>
<head>
  <title>Your PHP Page</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

  <link rel="stylesheet" href="/custom/workshop/css/style.css">
</head>
<body>
  <!-- Your PHP code and HTML content here -->
</body>
</html>

<?php

// Need to have following variables defined:
//  * $object (invoice, order, ...)
//  * $action
//  * $conf
//  * $langs
//  * $form

// if (empty($conf) || !is_object($conf)) {
// 	print "Error, template page can't be called as URL";
// 	exit;
// }
if (!is_object($form)) {
	$form = new Form($db);
}

print '<div class="container-fluid" style="text-align: center; color: grey; padding: 3px;">';
print '<h2>';
print "you have successfully completed your AC MOTOR Inspection";
print '</h2>';
print '<hr>';
print '<h4>';
print "Do you want to generate a WorkOrder for this Inspection?";
print '</h4>';
print '<form action="'.DOL_URL_ROOT.'/custom/workshop/inspectionlineitem_card.php?action=addline" method="post">';
print '<input type="hidden" name="token" value="'.newToken().'">';
print '<button type="submit" name="insert_button" class="btn btn-primary ml-ms-2 btn-lg" style="background-color: lightblue; padding: 10px 20px; border-radius: 5px;"  value="submit">Create WorkOrder</button>';
print '</form>';
print '</div>';

print '<div class="container-fluid"  style="text-align: center; color: grey; padding: 3px">';
print '<hr>';
print '<h2>';
print 'This Section is fo WorkOrder PDF Generation';
print '</h2>';
print '</div>';
?>
<div class="container-fluid">
  <table class="table">
    <thead>
      <tr class="liste_titre">
        <th colspan="5" class="formdoc liste_titre maxwidthonsmartphone text-center">
          <span class="d-block d-sm-inline">Doc template</span>
          <select id="model" class="form-control form-control-sm d-inline-block ml-sm-2" name="model">
            <option value="cyan" selected>cyan</option>
          </select>
          <input class="btn btn-primary ml-sm-2" id="builddoc_generatebutton" name="builddoc_generatebutton" type="submit" value="Generate">
        </th>
      </tr>
</thead>


  
<!-- JS CODE TO ENABLE select2 for id = model -->
		<script>
			$(document).ready(function () {
				$('#model').select2({
					dir: 'ltr',		width: 'resolve',		/* off or resolve */
					minimumInputLength: 0,
					language: select2arrayoflanguage,
					matcher: function (params, data) {
						if ($.trim(params.term) === "") {
							return data;
						}
						keywords = (params.term).split(" ");
						for (var i = 0; i < keywords.length; i++) {
							if (((data.text).toUpperCase()).indexOf((keywords[i]).toUpperCase()) == -1) {
								return null;
							}
						}
						return data;
					},
					theme: 'default minwidth75 maxwidth200',		/* to add css on generated html components */
					containerCssClass: ':all:',					/* Line to add class of origin SELECT propagated to the new <span class="select2-selection...> tag */
					selectionCssClass: ':all:',					/* Line to add class of origin SELECT propagated to the new <span class="select2-selection...> tag */
					dropdownCssClass: 'ui-dialog',
					templateResult: function (data, container) {	/* Format visible output into combo list */
	 					/* Code to add class of origin OPTION propagated to the new select2 <li> tag */
						if (data.element) { $(container).addClass($(data.element).attr("class")); }
						//console.log($(data.element).attr("data-html"));
						if (data.id == -1 && $(data.element).attr("data-html") == undefined) {
							return '&nbsp;';
						}
						if ($(data.element).attr("data-html") != undefined) return htmlEntityDecodeJs($(data.element).attr("data-html"));		// If property html set, we decode html entities and use this
						return data.text;
					},
					templateSelection: function (selection) {		/* Format visible output of selected value */
						if (selection.id == -1) return '<span class="placeholder">'+selection.text+'</span>';
						return selection.text;
					},
					escapeMarkup: function(markup) {
						return markup;
					}
				});
});
</script>

<!-- JS CODE TO ENABLE select2 for id = model -->
		<script>
			$(document).ready(function () {
				$('#model').select2({
					dir: 'ltr',		width: 'resolve',		/* off or resolve */
					minimumInputLength: 0,
					language: select2arrayoflanguage,
					matcher: function (params, data) {
						if ($.trim(params.term) === "") {
							return data;
						}
						keywords = (params.term).split(" ");
						for (var i = 0; i < keywords.length; i++) {
							if (((data.text).toUpperCase()).indexOf((keywords[i]).toUpperCase()) == -1) {
								return null;
							}
						}
						return data;
					},
					theme: 'default',		/* to add css on generated html components */
					containerCssClass: ':all:',					/* Line to add class of origin SELECT propagated to the new <span class="select2-selection...> tag */
					selectionCssClass: ':all:',					/* Line to add class of origin SELECT propagated to the new <span class="select2-selection...> tag */
					dropdownCssClass: 'ui-dialog',
					templateResult: function (data, container) {	/* Format visible output into combo list */
	 					/* Code to add class of origin OPTION propagated to the new select2 <li> tag */
						if (data.element) { $(container).addClass($(data.element).attr("class")); }
						//console.log($(data.element).attr("data-html"));
						if (data.id == -1 && $(data.element).attr("data-html") == undefined) {
							return '&nbsp;';
						}
						if ($(data.element).attr("data-html") != undefined) return htmlEntityDecodeJs($(data.element).attr("data-html"));		// If property html set, we decode html entities and use this
						return data.text;
					},
					templateSelection: function (selection) {		/* Format visible output of selected value */
						if (selection.id == -1) return '<span class="placeholder">'+selection.text+'</span>';
						return selection.text;
					},
					escapeMarkup: function(markup) {
						return markup;
					}
				});
});
</script>


		<script>
			$(document).ready(function () {
				$('#lang_id').select2({
					dir: 'ltr',		width: 'resolve',		/* off or resolve */
					minimumInputLength: 0,
					language: select2arrayoflanguage,
					matcher: function (params, data) {
						if ($.trim(params.term) === "") {
							return data;
						}
						keywords = (params.term).split(" ");
						for (var i = 0; i < keywords.length; i++) {
							if (((data.text).toUpperCase()).indexOf((keywords[i]).toUpperCase()) == -1) {
								return null;
							}
						}
						return data;
					},
					theme: 'default',		/* to add css on generated html components */
					containerCssClass: ':all:',					/* Line to add class of origin SELECT propagated to the new <span class="select2-selection...> tag */
					selectionCssClass: ':all:',					/* Line to add class of origin SELECT propagated to the new <span class="select2-selection...> tag */
					dropdownCssClass: 'ui-dialog',
					templateResult: function (data, container) {	/* Format visible output into combo list */
	 					/* Code to add class of origin OPTION propagated to the new select2 <li> tag */
						if (data.element) { $(container).addClass($(data.element).attr("class")); }
						//console.log($(data.element).attr("data-html"));
						if (data.id == -1 && $(data.element).attr("data-html") == undefined) {
							return '&nbsp;';
						}
						if ($(data.element).attr("data-html") != undefined) return htmlEntityDecodeJs($(data.element).attr("data-html"));		// If property html set, we decode html entities and use this
						return data.text;
					},
					templateSelection: function (selection) {		/* Format visible output of selected value */
						if (selection.id == -1) return '<span class="placeholder">'+selection.text+'</span>';
						return selection.text;
					},
					escapeMarkup: function(markup) {
						return markup;
					}
				});
});
</script>
<tbody>
      <tr class="oddeven">
        <td class="minwidth200 tdoverflowmax300">
          <span class="spanoverflow widthcentpercentminusx valignmiddle">
            <a class="documentdownload paddingright" href="/htdocs/document.php?modulepart=propal&amp;file=%28PROV15%29%2F%28PROV15%29.pdf&amp;entity=1" title="(PROV15).pdf">
              <i class="fa fa-file-pdf-o paddingright" title="File: (PROV15).pdf"></i>(PROV15).pdf
            </a>
          </span>
          <a class="pictopreview documentpreview" href="/htdocs/document.php?modulepart=propal&amp;attachment=0&amp;file=%28PROV15%29%2F%28PROV15%29.pdf&amp;entity=1" mime="application/pdf" target="_blank">
            <span class="fa fa-search-plus pictofixedwidth" style="color: gray"></span>
          </a>
        </td>
        <td class="nowraponall text-right">12 Kb</td>
        <td class="nowrap text-right">08/30/2023 09:50 AM</td>
        <td class="nowraponall">
          <a href="http://localhost/htdocs/document.php?hashp=zCe53S3kCzhEr3JPu5CWc1uaX31tE7P1" target="_blank" rel="noopener">
            <span class="fas fa-external-link-alt" style="color: blue;" title="File shared with a public link"></span>
          </a>
          <input type="text" class="form-control form-control-sm d-inline-block ml-2" id="downloadlink23" name="downloadexternallink" title="File shared with a public link" value="http://localhost/htdocs/document.php?hashp=zCe53S3kCzhEr3JPu5CWc1uaX31tE7P1">
            <script nonce="70e000a0">
               jQuery(document).ready(function () {
				    jQuery("#downloadlink23").click(function() { jQuery(this).select(); } );
				});
		    </script>
        </td>
        <td class="text-right nowraponall">
          <a class="reposition" href="/htdocs/comm/propal/card.php?id=15&amp;action=remove_file&amp;token=ad1dc43587f786bd1eaa03de1b84f8ab&amp;file=%28PROV15%29%2F%28PROV15%29.pdf&amp;entity=1">
            <span class="fas fa-trash" style="color: red;" title="Delete"></span>
          </a>
        </td>
      </tr>
    </tbody>
  </table>
</div>


    