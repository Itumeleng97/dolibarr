<!DOCTYPE html>
<html>
<head>
  <title>Your PHP Page</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

  <link rel="stylesheet" href="path/to/custom.css">
</head>
 
<?php

if (empty($conf) || !is_object($conf)) {
	print "Error, template page can't be called as URL";
	exit;
}
$Columns = array('Product_ID', 'Label', 'Status');
?>
  <div class="container">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th><?= $Columns[0]; ?></th>
          <th><?= $Columns[1]; ?></th>
          <th><?= $Columns[2]; ?></th>
        </tr>
      </thead>
      <tbody>
        <?php

        // Assuming $inspectionItems is an array of arrays
        $halfCount = count($inspectionItems) / 2; // Calculate the number of items to display (half of the total)

        foreach ($inspectionItems as $index => $item) {
            // Check if the index is less than the halfCount
                print '<input type="hidden" name="rowid_' . $index . '" value="' . $item['rowid'] . '">';

                print '<input type="hidden" name="fk_product' . $index . '" value="' . $item['fk_product'] . '">';
                print '<tr>';
                print '<td class="col-md-4">';
                print '<input type="text" name="prodid_' . $index . '" value="' . $item['prodid'] . '" readonly>';
                print '</td>';
                print '<td><input type="text" name="label_' . $index . '" value="' . $item['label'] . '" readonly></td>';
                print '<td>';
                print '<select name="status_'. $index .'">';
                print '<option value="Repair" ' . ($item['status'] == 'Repair' ? 'selected' : '') . '>Repair</option>';
                print '<option value="Replace" ' . ($item['status'] == 'Replace' ? 'selected' : '') . '>Replace</option>';
                print '<option value="Comply" ' . ($item['status'] == 'Comply' ? 'selected' : '') . '>Comply</option>';
                print '</select>';
                print '</td>';
                print '</tr>';


            
        }
        
        ?>

      </tbody>
    </table>
  </div>
 </div>


