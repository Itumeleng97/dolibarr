<!DOCTYPE html>
<html>
<head>
  <title>Your PHP Page</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="/custom/workshop/css/style.css">
</head>
<body>
  <div class="container">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Product_ID</th>
                <th>Label</th>
                <th>Status</th>
              
            </tr>
        </thead>
        <tbody>
            <?php
            // Provided data array
          

            // Loop through the data and populate the table rows
            foreach ($inspectionItems as $item) {
                print '<tr>';
                print '<td>' . $item['prodid'] . '</td>';
                print '<td>' . $item['label'] . '</td>';
                print '<td>' . $item['status'] . '</td>';
             
                print '</tr>';
            }
            ?>
        </tbody>
    </table>
  </div>
</body>
</html>
