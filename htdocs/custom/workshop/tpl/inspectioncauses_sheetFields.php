<!-- 
/* Copyright (C) 2017-2019  Laurent Destailleur  <eldy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */ -->
 <!DOCTYPE html>
<html>
<head>
  <title>Your PHP Page</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

  <link rel="stylesheet" href="/custom/workshop/css/style.css">
</head>
<body>
  <!-- Your PHP code and HTML content here -->
</body>
</html>

<?php

// Need to have following variables defined:
//  * $object (invoice, order, ...)
//  * $action
//  * $conf
//  * $langs
//  * $form

if (empty($conf) || !is_object($conf)) {
	print "Error, template page can't be called as URL";
	exit;
}
if (!is_object($form)) {
	$form = new Form($db);
}


//<!-- BEGIN PHP TEMPLATE commonfields_edit.tpl.php -->
$object2->fields = dol_sort_array($object2->fields, 'position');







$causes = selectCausesFromDatabase($db);
// llxHeader("", $langs->trans("WorkshopArea"));

// print load_fiche_titre($langs->trans("WorkshopArea"), '', 'workshop.png@workshop');

// print '<hr>';
print '<div class="container" style="text-align: center">';
print '<h3> ';
print '<bold>';
print "inspectionsheet Causes";
print '<bold>';
print '</h3>';
print '</div>';
print '<hr>';
print '<div class="container-fluid">';


foreach ($object2->fields as $key => $val) {
	// Discard if field is a hidden field on form
	if (abs($val['visible']) != 1 && abs($val['visible']) != 3) {
		continue;
	}

	if (array_key_exists('enabled', $val) && isset($val['enabled']) && !verifCond($val['enabled'])) {
		continue; // We don't want this field
	}

	print '<div class="checkboxes-container">'; // Start a container for the checkboxes

	foreach ($causes as $cause) {
		$causeValue = $cause[0]; // Get the cause value
	
		print '<label>';
		print '<input type="checkbox" name="causes[]" value="' . $causeValue . '"> ' . $causeValue;
		print '</label>';
	}
	print '</div>';
}



print '</div>';












	
 
?>
<!-- END PHP TEMPLATE commonfields_edit.tpl.php -->
