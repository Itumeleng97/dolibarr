

 <!DOCTYPE html>
<html>
<head>
  <title>Your PHP Page</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">


  <link rel="stylesheet" href="/custom/workshop/css/style.css">
</head>


<body>
  <!-- Your PHP code and HTML content here -->
</body>
</html>

<?php

// Protection to avoid direct call of template
if (empty($conf) || !is_object($conf)) {
	print "Error, template page can't be called as URL";
	exit;
}


//<!-- BEGIN PHP TEMPLATE commonfields_add.tpl.php -->
// Sorting the fields by position
// Sorting the fields by position
// Sorting the fields by position
$object->fields = dol_sort_array($object->fields, 'position');

// Get the handler origin

// Desired order of columns
$InspectionColumns = array('product_id', 'label', 'status', 'description','fk_product','repair_amount','replace_amount');

// Open a row div with Bootstrap classes
print '<div class="container-fluid">';
// print '<div class="row">';

// First column for the first table
print '<div class="row">';
print '<div class="col-md-6">';
print '<table class="table table-responsive table-bordered">'."\n";
print '<tr class="field_column">';
foreach ($InspectionColumns as $column) {
    if (!isset($object->fields[$column])) {
        continue; // Skip this column if it doesn't exist in $object->fields
    }

    $val = $object->fields[$column];

    // Use Bootstrap classes to style the table header cells
    if ($val['label'] !== 'fk_product' && $val['label' !== 'repair_amount'] && $val['label' !== 'replace_amount']) {
        print '<th class="col-4 titlefieldcreate';
        if (isset($val['notnull']) && $val['notnull'] > 0) {
            print ' fieldrequired';
        }
        if ($val['type'] == 'text' || $val['type'] == 'html') {
            print ' tdtop';
        }
        if ($val['type'] == 'integer' ) {
            print ' style="width:2px"';
        }
        print '">';
        if (!empty($val['help'])) {
            print $form->textwithpicto($langs->trans($val['label']), $langs->trans($val['help']));
        } else {
            print $langs->trans($val['label']);
        }
        print '</th>';
    }
}
print '</tr>';




// Continue with the rest of the table rows for the first table

$totalItems = count($labels);
$halfCount = ceil($totalItems / 2);


foreach ($labels as $index => $row) {



    if ($index >= $halfCount) {
        break;
    }

    print '<tr>';
    foreach ($InspectionColumns as $column) {
        if ($column === 'repair_amount') {
            // The hidden input field for the last column
            print '<input type="hidden" name="repair_amount' . $index . '" value="' . $row[$column] . '">';
        }
        
        
        if($column === 'replace_amount'){
            print '<input type="hidden" name="replace_amount' . $index . '" value="' . $row[$column] . '">';

        } 
        if($column === 'fk_product'){
            print '<input type="hidden" name="fk_product_' . $index . '" value="' . $row[$column] . '">';

        } 
        if (!isset($object->fields[$column])) {
                continue;
            }

            $val = $object->fields[$column];

            print '<td class="col-md-4">';

            if ($column === 'product_id') {
                print '<input type="text" name="product_id_' . $index . '" value="' . $row[$column] . '" readonly>';
            } elseif ($column === 'label') {
                print '<input type="text" name="label_' . $index . '" value="' . $row[$column] . '" readonly>';
            } elseif ($column === 'status') {
                $statusName = "status_$index";        
                print '<select name="' . $statusName . '">';
                print '<option value="Comply"' . ($row[$column] === 'Comply' ? ' selected' : '') . '>Comply</option>';
                print '<option value="Repair"' . ($row[$column] === 'Repair' ? ' selected' : '') . '>Repair</option>';
                print '<option value="Replace"' . ($row[$column] === 'Replace' ? ' selected' : '') . '>Replace</option>';
                print '</select>';
            } elseif ($column === 'description') {
               
                $inputName = "description_$index"; 
                print '<input type="text" name="' . $inputName . '" value="" >'; 
            }

            print '</td>';
        }
    }
    print '</tr>';




print '</table>';
print '</div>';

// Second column for the second table
print '<div class="col-md-6">';
print '<table class="table table-responsive table-bordered">'."\n";
print '<tr class="field_column">';
foreach ($InspectionColumns as $column) {
    if (!isset($object->fields[$column])) {
        continue; // Skip this column if it doesn't exist in $object->fields
    }

    $val = $object->fields[$column];

    // Use Bootstrap classes to style the table header cells
    if ($val['label'] !== 'fk_product'  && $val['label' !== 'repair_amount'] && $val['label' !== 'replace_amount']) {
        print '<th class="col-4 titlefieldcreate';
        if (isset($val['notnull']) && $val['notnull'] > 0) {
            print ' fieldrequired';        }
        if ($val['type'] == 'text' || $val['type'] == 'html') {
            // print ' tdtop';
        }
        print '">';
        if (!empty($val['help'])) {
            print $form->textwithpicto($langs->trans($val['label']), $langs->trans($val['help']));
        } else {
            print $langs->trans($val['label']);
        }
        print '</th>';
    }
}
print '</tr>';


// Continue with the rest of the table rows for the second table
foreach ($labels as $index => $row) {
    if ($index < $halfCount ) {
        continue;
    }

    print '<tr>';
    foreach ($InspectionColumns as $column) {
        if ($column === 'repair_amount') {
            // The hidden input field for the last column
            print '<input type="hidden" name="repair_amount' . $index . '" value="' . $row[$column] . '">';
        }
        
        
        if($column === 'replace_amount'){
            print '<input type="hidden" name="replace_amount' . $index . '" value="' . $row[$column] . '">';

        } 
        if($column === 'fk_product'){
            print '<input type="hidden" name="fk_product_' . $index . '" value="' . $row[$column] . '">';

        }
        if (!isset($object->fields[$column])) {
                continue;
            }

            $val = $object->fields[$column];

            print '<td class="col-md-4">';

            if ($column === 'product_id') {
                print '<input type="text" name="product_id_' . $index . '" value="' . $row[$column] . '" readonly>';
            } elseif ($column === 'label') {
                print '<input type="text" name="label_' . $index . '" value="' . $row[$column] . '" readonly>';
            } elseif ($column === 'status') {
                $statusName = "status_$index";        
                print '<select name="' . $statusName . '">';
                print '<option value="Comply"' . ($row[$column] === 'Comply' ? ' selected' : '') . '>Comply</option>';
                print '<option value="Repair"' . ($row[$column] === 'Repair' ? ' selected' : '') . '>Repair</option>';
                print '<option value="Replace"' . ($row[$column] === 'Replace' ? ' selected' : '') . '>Replace</option>';
                print '</select>';
            } elseif ($column === 'description') {
                $inputName = "description_$index"; 
                print '<input type="text" name="description_' . $inputName . '" value="" >'; 

            }

            print '</td>';
        }
    }
    print '</tr>';


print '</table>';
print '</div>';

// Close the row div
print '</div>';

print '</div>';


	


?>
<!-- END PHP TEMPLATE commonfields_add.tpl.php -->
