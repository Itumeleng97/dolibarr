<!-- 
/* Copyright (C) 2017-2019  Laurent Destailleur  <eldy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */ -->
<!DOCTYPE html>
<html>
<head>
  <title>Your PHP Page</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

  <link rel="stylesheet" href="path/to/custom.css">
</head>
<body>
  <!-- Your PHP code and HTML content here -->


<?php

// Need to have following variables defined:
//  * $object (invoice, order, ...)
//  * $action
//  * $conf
//  * $langs
//  * $form

if (empty($conf) || !is_object($conf)) {
	print "Error, template page can't be called as URL";
	exit;
}
if (!is_object($form)) {
	$form = new Form($db);
}


//<!-- BEGIN PHP TEMPLATE commonfields_edit.tpl.php -->
$object->fields = dol_sort_array($object->fields, 'position');

	



// print load_fiche_titre($langs->trans("WorkshopArea"), '', 'workshop.png@workshop');
print '<div class="container-fluid">';

print '<form method="post">';
print '<div class="row">';

foreach ($object->fields as $key => $val) {
	// Discard if field is a hidden field on form
	if (abs($val['visible']) != 1 && abs($val['visible']) != 3) {
		continue;
	}

	if (array_key_exists('enabled', $val) && isset($val['enabled']) && !verifCond($val['enabled'])) {
		continue; // We don't want this field
	}

	print '<div class="col-md-3">';
	print '<div class="form-group row">';
	print '<label class="col-md-4 col-form-label';


	if (isset($val['notnull']) && $val['notnull'] > 0) {
		print ' fieldrequired';
	}
	print '">';
	if (!empty($val['help'])) {
		print $form->textwithpicto($langs->trans($val['label']), $langs->trans($val['help']));
	} else {
		print $langs->trans($val['label']);
	}
	print '</label>';

	print '<div class="col-md-4">';
	print '<div class="valuefieldcreate" >';
	if (!empty($val['picto'])) {
		print img_picto('', $val['picto'], '', false, 0, 0, '', 'pictofixedwidth');
	}

	 else {
		if (in_array($val['type'], array('int', 'integer'))) {
			$value = GETPOST($key, 'int');
		} elseif ($val['type'] == 'double') {
			$value = price2num(GETPOST($key, 'alphanohtml'));
		} elseif (preg_match('/^text/', $val['type'])) {
			$tmparray = explode(':', $val['type']);
			if (!empty($tmparray[1])) {
				$check = $tmparray[1];



			} else {
				$check = 'nohtml';
			}
			$value = GETPOST($key, $check);
		} elseif (preg_match('/^html/', $val['type'])) {
			$tmparray = explode(':', $val['type']);
			if (!empty($tmparray[1])) {
				$check = $tmparray[1];
			} else {
				$check = 'restricthtml';
			}
			$value = GETPOST($key, $check);
		} elseif ($val['type'] == 'date') {
			$value = dol_mktime(12, 0, 0, GETPOST($key.'month', 'int'), GETPOST($key.'day', 'int'), GETPOST($key.'year', 'int'));
		} elseif ($val['type'] == 'datetime') {
			$value = dol_mktime(GETPOST($key.'hour', 'int'), GETPOST($key.'min', 'int'), 0, GETPOST($key.'month', 'int'), GETPOST($key.'day', 'int'), GETPOST($key.'year', 'int'));
		} elseif ($val['type'] == 'boolean') {
			$value = (GETPOST($key) == 'on' ? 1 : 0);
		} elseif ($val['type'] == 'price') {
			$value = price2num(GETPOST($key));
		} elseif ($key == 'lang') {
			$value = GETPOST($key, 'aZ09');
		} else {
			$value = GETPOST($key, 'alphanohtml');
		}
	}

	if (!empty($val['noteditable'])) {
		print $object->showOutputField($val, $key, $value, '', '', '', 0);
	} else {
		if ($key == 'lang') {
			print img_picto('', 'language', 'class="pictofixedwidth"');
			print $formadmin->select_language($value, $key, 0, null, 1, 0, 0, 'minwidth300', 2);
		} else {
			print $object->showInputField($val, $key, $value, '', '', '', 0);
		}
	}

	print '</div>';
	print '</div>';
	print '</div>';
	print '</div>';
}
print '</form>';
print '</div>';


?>




























	
 
<!-- END PHP TEMPLATE commonfields_edit.tpl.php -->
