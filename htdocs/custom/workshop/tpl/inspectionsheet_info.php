<!DOCTYPE html>
<html>
<head>
  <title>Your PHP Page</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">


  <link rel="stylesheet" href="/custom/workshop/css/style.css">
</head>


<body>
  <!-- Your PHP code and HTML content here -->


<?php

// Protection to avoid direct call of template
if (empty($conf) || !is_object($conf)) {
	print "Error, template page can't be called as URL";
	exit;
}




dol_include_once('/workshop/lib/workshop_inspectionsheet.lib.php');


// Sample InspectionColumns array
$InspectionColumns = array('product_id', 'label', 'status', 'description', 'fk_product');





?>
<div class="container mt-4">
        <?php
        // Sample result array
     
        $results = selectInspectionInfo($db);
        // Sample InspectionColumns array
        $InspectionColumns = array('product_id', 'label', 'status', 'description');
        ?>

        <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <?php foreach ($InspectionColumns as $column) : ?>
                        <th><?php echo ucfirst($column); ?></th>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($results as $result) : ?>
                    <tr>
                        <?php foreach ($InspectionColumns as $column) : ?>
                            <td>
                                <?php
                                // Check if the key exists in the result array
                                if (isset($result[$column])) {
                                    echo $result[$column];
                                } else {
                                    echo '-';
                                }
                                ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>



    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>