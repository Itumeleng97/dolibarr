
<!DOCTYPE html>
<html>
<head>
  <title>Your PHP Page</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

  <link rel="stylesheet" href="path/to/custom.css">
</head>
<body>
 


<?php

// Need to have following variables defined:
//  * $object (invoice, order, ...)
//  * $action
//  * $conf
//  * $langs
//  * $form

if (empty($conf) || !is_object($conf)) {
	print "Error, template page can't be called as URL";
	exit;
}
if (!is_object($form)) {
	$form = new Form($db);
}


        print '<br>';
        // print '<hr>';

            // Assuming $data is your array with label and value pairs
        print  '<div class="container-fluid">';
        print  '<form method="post">';
        print  '<div class="row">';

        $labelValuePairs = array_filter($workshopData, 'is_string', ARRAY_FILTER_USE_KEY);

        foreach ($labelValuePairs as $key => $value) {
            // Display label and value in smaller bordered boxes
            print  '<div class="col-md-3 col-sm-4">';
            print  '<div class="border p-2 mb-2 d-flex justify-content-between">';
            
            print  '<div class="label">';
            print  htmlspecialchars($key, ENT_QUOTES, 'UTF-8'); // Display label safely
            print  '</div>';
            print '<br>';

            print  '<span class="value">';
            print  htmlspecialchars($value, ENT_QUOTES, 'UTF-8'); // Display value safely
            print  '</span>';


            print  '</div>';
            print  '</div>';
        }

        print  '</div>';
        print  '</form>';
        print  '</div>';

        

